import engine.renderer.RendererOptions

return new RendererOptions(
        screenWidth: 1024,
        screenHeight: 600,
        waitForVerticalSync: false,
        limitFPS: false,
        maxFPS: 60,
        voxelRenderSize: 64
    )
