# README #

### What is this repository for? ###

Draco is my very own isometric voxel engine project.

### How do I get set up? ###

Just type mvn install and you will get a running demo game jar. Maven cares about dependencies.

I'm using IntelliJ IDEA as my favorite IDE but the project solely depends on Maven. There are no IDE specific files. Just import the pom.xml into your favorite IDE.

ZGC Garbage Collector is recommended for newer JVMs:

> -XX:+UseZGC

### Contribution guidelines ###

* None yet

### Who do I talk to? ###

* hugo_boffin

### How does it look like? ###

![screenshot.png](https://bitbucket.org/repo/89rKLE/images/1699407685-screenshot.png)

### How do I ... ? ###

Create an executable: mvn package
Create Site (Javadoc, JQAssistant, ...): mvn site
Display code graph: mvn jqassistant:server