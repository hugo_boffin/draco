/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Generic Singleton for loading resources (files from disk, jar, network, ...)
 * <p>
 * Search Order (Fixed for the moment)
 *      1) Load File from current directory
 *      2) Load File from resources directory
 *      3) Load File from jar
 *
 * There is no resource caching at the moment (this could be handy if the same resource is request multiple times)
 *
 * @author  Christian Sommer
 * @since   3/3/2015
 */
public class Loader {
    private static final Logger logger = LogManager.getLogger(Loader.class.getName());
    private static Loader instance;

    /**
     * Returns the instance of this singleton.
     *
     * @return instance
     */
    public static Loader get() {
        if (instance == null) {
            instance = new Loader();
        }
        return instance;
    }

    /**
     * Loads and returns a resource.
     *
     * @param uid resource uid
     * @return loaded resource
     */
    public InputStream getResource(String uid) {
        InputStream stream = null;

        try {
            logger.info("Loading \"" + uid + "\"...");
            stream = new FileInputStream(uid);
        } catch (FileNotFoundException e1) {
            try {
                logger.info("   From resources?...");
                stream = new FileInputStream("src\\main\\resources\\" + uid);
            } catch (FileNotFoundException e2) {
                logger.info("   From jar?...");
                stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(uid);

                if (stream == null)
                    logger.warn("   Not Found!");
            }
        }

        return stream;
    }
}
