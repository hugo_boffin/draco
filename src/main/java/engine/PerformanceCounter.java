/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine;

/**
 * This class implements a high performance counter for accurate game timing and movement.
 *
 * @author  Christian Sommer
 * @since   6/13/2014
 */
public class PerformanceCounter {
    private long lastTime = -1;
    private double scale = 0.0;

    /**
     * Simple Animation scaling based upon elapsed time between the last two frames.
     * This method needs to be called every frame.
     */
    public void tick() {
        if (lastTime == -1)
            lastTime = System.nanoTime();

        long nowTime = System.nanoTime();
        long elapsedTime = nowTime - lastTime;
        lastTime = nowTime;
        scale = (double) elapsedTime * 0.000000001;
    }

    /**
     * Returns the scaling value for game animations
     *
     * @return scaling value
     */
    public double getScale() {
        return scale;
    }

    /**
     * After a long game pause it can be handy to reset the counter. This prevents "jumps".
     */
    public void reset() {
        lastTime = -1;
    }
}
