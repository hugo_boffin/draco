/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine;

import com.jogamp.opengl.GLAutoDrawable;

/**
 * GameDelegate fills a game with life. Here you load/create
 * all assets, bind input devices, and so on.
 *
 * @author  Christian Sommer
 * @since   3/4/2015
 */
public interface GameDelegate {
    /**
     * The string returned is used for the window title.
     *
     * @return Game Title
     */
    String getTitle();

    /**
     * Actions that need to be done before a GL context will be created.
     */
    void initialize();

    /**
     * Method will be invoked after the GL context has been created.
     * Here you can allocate all initial assets, textures, meshes
     * and all the other context specific things.
     *
     * @param drawable GL context
     */
    void loadContent(GLAutoDrawable drawable);

    /**
     * Method will be invoked when the window size has changed.
     *
     * @param drawable     GL context
     * @param x            position x-coordinate
     * @param y            position y-coordinate
     * @param width        surface width
     * @param height       surface height
     */
    void reshape(GLAutoDrawable drawable, int x, int y, int width, int height);

    /**
     * Per Frame method. Invoked before draw. Here you can update your game logic, timers, etc.
     */
    void update();

    /**
     * Per Frame Method. Invoked after update. Here you render everything.
     * @param drawable GL context
     */
    void draw(GLAutoDrawable drawable);

    /**
     * This method will be invoked on GL context dispose. Here you clean up all unmanaged resources (textures, etc.)
     *
     * @param drawable GL context
     */
    void dispose(GLAutoDrawable drawable);
}
