/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine;

import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.util.AnimatorBase;
import com.jogamp.opengl.util.FPSAnimator;
import engine.input.Input;
import engine.renderer.RendererOptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jogamp.nativewindow.WindowClosingProtocol;
import com.jogamp.opengl.*;

/**
 * This is the main class for creating a game. It sets up all essential systems.
 * Create your own @link GameDelegate implementation for a concrete game.
 *
 * @author  Christian Sommer
 * @since   3/4/2015
 */
public class Game {
    private final Input input;
    private final AnimatorBase animator;
    private final GLWindow glWindow;
    private final RendererOptions rendererOptions;
    private final Logger logger;
    private boolean isRunning = false;

    /**
     * Creates a game class and sets up basic things. The game starts up through the {@link engine.Game#run} method.
     */
    public Game() {
        System.setProperty("log4j.configurationFile", "log4j2.xml");
        logger = LogManager.getLogger(this.getClass().getName());

        rendererOptions = RendererOptions.get();

        GLProfile glp = GLProfile.getDefault();
        logger.info(glp);

        GLCapabilities caps = new GLCapabilities(glp);
        logger.info(caps);

        glWindow = GLWindow.create(caps);
        logger.info(glWindow);

        animator = rendererOptions.limitFPS
                ? new FPSAnimator(glWindow,rendererOptions.maxFPS,true /*TODO: Why not false anymore*/)
                : new Animator(glWindow);
        animator.setModeBits(false, Animator.MODE_EXPECT_AWT_RENDERING_THREAD);
        animator.setUpdateFPSFrames(FPSCounter.DEFAULT_FRAMES_PER_INTERVAL, null);

        glWindow.setSize(rendererOptions.screenWidth, rendererOptions.screenHeight);

        input = new Input(glWindow);
    }

    /**
     * Returns the input class.
     *
     * @return input
     */
    public Input getInput() {
        return input;
    }

    /**
     * Returns the Animator.
     *
     * @return animator
     */
    public AnimatorBase getAnimator() {
        return animator;
    }

    /**
     * Returns the glWindow
     *
     * @return glWindow
     */
    public GLWindow getGlWindow() {
        return glWindow;
    }

    /**
     * Starts the game.
     *
     * @param gameDelegate Concrete Game delegate.
     */
    public void run(GameDelegate gameDelegate) {
        if (isRunning) {
            logger.warn("Game is already running!");
            return;
        }
        isRunning = true;

        gameDelegate.initialize();

        glWindow.setTitle(gameDelegate.getTitle());

        glWindow.addGLEventListener(new GLEventListener() {
            @Override
            public void init(GLAutoDrawable glAutoDrawable) {
                if (RendererOptions.get().debugGL) {
                    if (glAutoDrawable.getGL().isGL4bc()) {
                        final GL4bc gl4bc = glAutoDrawable.getGL().getGL4bc();
                        glAutoDrawable.setGL(new DebugGL4bc(gl4bc));
                    } else {
                        if (glAutoDrawable.getGL().isGL4()) {
                            final GL4 gl4 = glAutoDrawable.getGL().getGL4();
                            glAutoDrawable.setGL(new DebugGL4(gl4));
                        } else {
                            if (glAutoDrawable.getGL().isGL3bc()) {
                                final GL3bc gl3bc = glAutoDrawable.getGL().getGL3bc();
                                glAutoDrawable.setGL(new DebugGL3bc(gl3bc));
                            } else {
                                if (glAutoDrawable.getGL().isGL3()) {
                                    final GL3 gl3 = glAutoDrawable.getGL().getGL3();
                                    glAutoDrawable.setGL(new DebugGL3(gl3));
                                } else {
                                    if (glAutoDrawable.getGL().isGL2()) {
                                        final GL2 gl2 = glAutoDrawable.getGL().getGL2();
                                        glAutoDrawable.setGL(new DebugGL2(gl2));
                                    }
                                }
                            }
                        }
                    }
                }

                gameDelegate.loadContent(glAutoDrawable);
            }

            @Override
            public void dispose(GLAutoDrawable glAutoDrawable) {
                gameDelegate.dispose(glAutoDrawable);
            }

            @Override
            public void display(GLAutoDrawable glAutoDrawable) {
                gameDelegate.update();
                gameDelegate.draw(glAutoDrawable);
            }

            @Override
            public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
                gameDelegate.reshape(glAutoDrawable, x, y, width, height);
            }
        });

        glWindow.setDefaultCloseOperation(WindowClosingProtocol.WindowClosingMode.DISPOSE_ON_CLOSE);
        glWindow.setWindowDestroyNotifyAction(() -> {
            logger.info("Received window destroy");
            animator.stop();
            glWindow.destroy();
        });

        animator.start();
        glWindow.setVisible(true);
    }
}
