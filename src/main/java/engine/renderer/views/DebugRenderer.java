/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.views;

import engine.renderer.blitter.Blitter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import util.math.VectorF3;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import java.util.stream.IntStream;

/**
 * This singleton class provides some easy to use debug rendering methods, like drawing lines, rectangles and text.
 * The rendering done here is not very optimized and for debugging purposes only.
 *
 * @author  Christian Sommer
 * @since   2/3/2015
 */
public class DebugRenderer implements Renderer {
    private static final Logger logger = LogManager.getLogger(DebugRenderer.class.getName());
    private static DebugRenderer debugRenderer;
    private final int MAX_LINES = 500;
    private final Line[] lines = new Line[MAX_LINES];
    private int lineCount = 0;

    /**
     * Creates an DebugRenderer.
    */
    public DebugRenderer() {
        IntStream.range(0, MAX_LINES).forEach(i -> lines[i] = new Line());
    }

    /**
     * Returns the singleton instance
     *
     * @return DebugRenderer instance
     */
    public static DebugRenderer get() {
        if (debugRenderer == null) {
            debugRenderer = new DebugRenderer();
        }
        return debugRenderer;
    }

    /**
     * Draws a colored line.
     *
     * @param x0 start-x
     * @param y0 start-y
     * @param x1 end-x
     * @param y1 end-y
     * @param color color
     * @param width line thickness
     */
    public void drawLine(int x0, int y0, int x1, int y1, int color, int width) {
        if (lineCount == MAX_LINES) {
            logger.warn("Too much debug geometry. Increase MAX_* values");
            return;
        }
        lines[lineCount].x0 = x0;
        lines[lineCount].y0 = y0;
        lines[lineCount].x1 = x1;
        lines[lineCount].y1 = y1;
        lines[lineCount].color = color;
        lines[lineCount].width = width;
        lineCount++;
    }

    /**
     * Draws a rectangle.
     *
     * @param x0 topleft-x
     * @param y0 topleft-y
     * @param x1 bottomright-x
     * @param y1 bottomright-y
     * @param color color
     * @param width line thickness
     */
    public void drawRect(int x0, int y0, int x1, int y1, int color, int width) {
        drawLine(x0, y0, x0, y1, color, width);
        drawLine(x0, y0, x1, y0, color, width);
        drawLine(x1, y0, x1, y1, color, width);
        drawLine(x0, y1, x1, y1, color, width);
    }

    @Override
    public void render(GLAutoDrawable drawable, GL2 gl, Blitter blitter, VectorF3 cameraPosition) {
        gl.glEnable(GL.GL_LINE_SMOOTH);
        gl.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_NICEST);

        for (int i = 0; i < lineCount; i++) {
            final Line line = lines[i];
            gl.glLineWidth(line.width);
            gl.glBegin(GL.GL_LINES);
            gl.glColor4ub((byte) ((line.color >> 24) & 0xFF),
                    (byte) ((line.color >> 16) & 0xFF),
                    (byte) ((line.color >> 8) & 0xFF),
                    (byte) ((line.color) & 0xFF));
            gl.glVertex2f(line.x0, line.y0);
            gl.glVertex2f(line.x1, line.y1);
            gl.glEnd();
        }

        flush();
    }

    private void flush() {
        lineCount = 0;
    }

    private static class Line {
        public int x0;
        public int y0;
        public int x1;
        public int y1;
        public int color;
        public int width;

        public Line(int x0, int y0, int x1, int y1, int color, int width) {
            this.x0 = x0;
            this.y0 = y0;
            this.x1 = x1;
            this.y1 = y1;
            this.color = color;
            this.width = width;
        }

        public Line() {
            this.x0 = 0;
            this.y0 = 0;
            this.x1 = 0;
            this.y1 = 0;
            this.color = 0;
            this.width = 3;
        }
    }
}
