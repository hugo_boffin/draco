/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.views;

/**
 * Base Interface for all Terrain Debug Renderers.
 *
 * @author  Christian Sommer
 * @since   1/22/2015
 */
public interface TerrainDebugRenderer extends TerrainRenderer {
    /**
     * Returns the current coordinate display.
     *
     * @return coordinate display.
     */
    CoordinatesDisplay getCoordinatesDisplay();

    /**
     * Sets a specific coordinate display.
     *
     * @param coordinatesDisplay type of display
     */
    void setCoordinatesDisplay(CoordinatesDisplay coordinatesDisplay);

    /**
     * Rotates coordinate display.
     */
    void toggleCoordinatesDisplay();

    /**
     * All Possible coordinate display types.
     */
    enum CoordinatesDisplay {
        /**
         * No coordinate display
         */
        NONE,
        /**
         * Display Real Coordinates (square)
         */
        REAL,
        /**
         * Display Mapped Coordinates (diamond)
         */
        MAPPED
    }
}
