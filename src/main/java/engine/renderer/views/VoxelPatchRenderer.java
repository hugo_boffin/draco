/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.views;

import engine.renderer.blitter.Blit;
import engine.renderer.blitter.Blitter;
import engine.renderer.blitter.Map;
import model.Voxel;
import model.VoxelPatch;
import util.tiles.GroundType;

import com.jogamp.opengl.GL2;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Implements a voxel patch renderer.
 *
 * @author  Christian Sommer
 * @since   6/16/2014
 */
public class VoxelPatchRenderer extends BasicVoxelPatchRenderer {
    private final ArrayList<VoxelRenderer> voxelRenderers = new ArrayList<>();
    private final ArrayList<Map> voxelMaps;
    private final Comparator<Blit> mapComparator = (o1, o2) -> {
        /*
         * Comparator for blits - Order:
         *   1) depth value
         *   2) y position
         *   3) texture id
         */

        if (o1.depthValue > o2.depthValue)
            return +1;
        if (o1.depthValue < o2.depthValue)
            return -1;

        if (o1.pos_y > o2.pos_y)
            return +1;
        if (o1.pos_y < o2.pos_y)
            return -1;

        if (o1.map.hashCode() > o2.map.hashCode())
            return +1;
        if (o1.map.hashCode() < o2.map.hashCode())
            return -1;

        return 0;
    };
    private boolean autoFlush = true;

    /**
     * Creates a VoxelPatchRenderer.
     *
     * @param groundTypes Defintion of all possible block types
     * @param voxelMaps Texture Maps for all block types
     */
    public VoxelPatchRenderer(List<GroundType> groundTypes, ArrayList<Map> voxelMaps) {
        this.voxelMaps = voxelMaps;

        VoxelRenderer defaultVoxelRenderer = new DefaultVoxelRenderer(this);
        VoxelRenderer waterVoxelRenderer = new WaterVoxelRenderer(this);

        groundTypes.forEach((g) -> {
            if (g.Name.equals("water"))
                voxelRenderers.add(waterVoxelRenderer);
            else
                voxelRenderers.add(defaultVoxelRenderer);
        });
    }

    @Override
    protected void startRendering() {

    }

    @Override
    protected void finishRendering() {
        if (autoFlush)
            flush(gl, blitter);
    }

    @Override
    protected boolean renderVoxel(GL2 gl, Blitter blitter, VoxelPatch patch,
                                  int x, int y, int z, Voxel voxel, int mx, int my) {
        selectVoxelRenderer(voxel).render(gl, blitter, patch, x, y, z, voxel, mx, my);

        return voxel.index == 15;
    }

    /**
     * Flushes the blitter buffer, using a comparator.
     *
     * @param gl GL context
     * @param blitter Blitter
     */
    public void flush(GL2 gl, Blitter blitter) {
        blitter.flush(gl, mapComparator);
    }

    /**
     * Returns the appropriate texture map for a given voxel.
     *
     * @param voxel voxel
     * @return appropriate texture map
     */
    public Map selectVoxelMap(final Voxel voxel) {
        return voxelMaps.get(voxel.base);
    }

    /**
     * Returns an appropriate renderer for a given voxel.
     *
     * @param voxel voxel
     * @return appropriate renderer
     */
    public VoxelRenderer selectVoxelRenderer(final Voxel voxel) {
        return voxelRenderers.get(voxel.base);
    }

    /**
     * Returns true if auto flush is enabled.
     *
     * @return true if auto flush is enabled
     */
    public boolean isAutoFlush() {
        return autoFlush;
    }

    /**
     * Sets auto flush.
     *
     * @param autoFlush automatically flush blitter on finishRendering
     */
    public void setAutoFlush(boolean autoFlush) {
        this.autoFlush = autoFlush;
    }
}
