/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.views;

import com.jogamp.opengl.util.awt.TextRenderer;
import engine.renderer.blitter.Blitter;
import model.DiamondMapping;
import model.Terrain;
import model.Voxel;
import model.VoxelPatch;
import util.math.VectorI3;

import com.jogamp.opengl.GL2;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Debug Renderer for {@link model.StreamingTerrain}. Can visualize cell size, index and voxel coordinates.
 *
 * @author  Christian Sommer
 * @since   9/5/2014
 */
public class StreamingTerrainDebugRenderer extends BasicStreamingTerrainRenderer implements TerrainDebugRenderer {
    private final TextRenderer textRenderer;
    private final TextRenderer textRendererBig;
    private VectorI3 nodePos;
    private CoordinatesDisplay coordinatesDisplay = CoordinatesDisplay.NONE;
    private final BasicVoxelPatchRenderer voxelPatchRenderer = new BasicVoxelPatchRenderer() {
        private final DiamondMapping mapping = new DiamondMapping();

        @Override
        protected void startRendering() {
        }

        @Override
        protected void finishRendering() {
        }

        @Override
        protected boolean renderVoxel(GL2 gl, Blitter blitter, VoxelPatch patch,
                                      int x, int y, int z, Voxel voxel, int mx, int my) {
            if (coordinatesDisplay == CoordinatesDisplay.NONE)
                return true;

            x += terrain.getCellSize().x * nodePos.x;
            y += terrain.getCellSize().y * nodePos.y;
            z += terrain.getCellSize().z * nodePos.z;

            final String loc = coordinatesDisplay == CoordinatesDisplay.REAL ? "(" + x + "," + y + "," + z + ")" : mapping.map(x, y, z).toString();
            Rectangle2D bounds = textRenderer.getBounds(loc);
            textRenderer.draw(loc,
                    VOXEL_SIZE / 2 + mx - (int) bounds.getWidth() / 2,
                    drawable.getSurfaceHeight() -
                            (VOXEL_SIZE / 4 + my + (int) bounds.getHeight() / 2));
            return true;
        }
    };

    /**
     * Creates a Streaming Terrain Debug Renderer.
     *
     * @param gl GL context
     * @param terrain Terrain to debug
     */
    public StreamingTerrainDebugRenderer(GL2 gl, Terrain terrain) {
        super(gl, terrain);

        textRenderer = new TextRenderer(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
        textRendererBig = new TextRenderer(new Font(Font.SANS_SERIF, Font.PLAIN, 20));
    }

    @Override
    protected void finishRendering() {
        textRenderer.endRendering();

        final int ox = nodePos.x * patchPixelSize.x - (int) cameraPosition.x + 15;
        final int oy = nodePos.y * patchPixelSize.y - (int) cameraPosition.y + 15;

        Rectangle2D bounds;
        final String text = nodePos.toString();
        bounds = textRenderer.getBounds(text);

        textRendererBig.beginRendering(drawable.getSurfaceWidth(), drawable.getSurfaceHeight());
        textRendererBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        textRendererBig.draw(text, ox, drawable.getSurfaceHeight() - oy - (int) bounds.getHeight());
        textRendererBig.endRendering();
    }

    @Override
    protected void renderPatch(VoxelPatch patch, VectorI3 nodePos) {
        this.nodePos = nodePos;

        final int LINE_WIDTH = 3;
        final int ox = nodePos.x * patchPixelSize.x - (int) cameraPosition.x + LINE_WIDTH;
        final int oy = nodePos.y * patchPixelSize.y - (int) cameraPosition.y + LINE_WIDTH;
        DebugRenderer.get().drawRect(ox, oy, ox + patchPixelSize.x - LINE_WIDTH * 2, oy + patchPixelSize.y - LINE_WIDTH * 2,
                ((nodePos.x + nodePos.y) % 2 == 0) ? 0xFFFF00FF : 0x00FFFFFF, LINE_WIDTH);

        voxelPatchRenderer.render(drawable, gl, blitter, cameraPosition, patch, nodePos.x * patchPixelSize.x, nodePos.y * patchPixelSize.y);
    }

    @Override
    protected void startRendering() {
        textRenderer.beginRendering(drawable.getSurfaceWidth(), drawable.getSurfaceHeight());
        textRenderer.setColor(1.0f, 1.0f, 1.0f, 1.0f);
    }

    @Override
    public CoordinatesDisplay getCoordinatesDisplay() {
        return coordinatesDisplay;
    }

    @Override
    public void setCoordinatesDisplay(CoordinatesDisplay coordinatesDisplay) {
        this.coordinatesDisplay = coordinatesDisplay;
    }

    @Override
    public void toggleCoordinatesDisplay() {
        switch (coordinatesDisplay) {
            case NONE -> coordinatesDisplay = CoordinatesDisplay.REAL;
            case REAL -> coordinatesDisplay = CoordinatesDisplay.MAPPED;
            case MAPPED -> coordinatesDisplay = CoordinatesDisplay.NONE;
        }
    }

    @Override
    public int getVisibleCellCount() {
        return -1;
    }
}
