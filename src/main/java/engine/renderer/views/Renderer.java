/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.views;

import engine.renderer.blitter.Blitter;
import util.math.VectorF3;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * Base Interface for all Renderers (Views)
 *
 * @author  Christian Sommer
 * @since   6/16/2014
 */
public interface Renderer {
    /**
     * Render is called every frame and should update the screen / gpu things.
     *
     * @param drawable GL drawable
     * @param gl GL context
     * @param blitter Blitter
     * @param cameraPosition camera
     */
    void render(GLAutoDrawable drawable, GL2 gl, Blitter blitter, VectorF3 cameraPosition);
}
