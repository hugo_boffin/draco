/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.views;

import engine.renderer.blitter.Blitter;
import engine.renderer.blitter.Map;
import model.Voxel;
import model.VoxelPatch;
import picking.MousePicking;
import util.math.VectorI3;

import com.jogamp.opengl.GL2;
import java.awt.*;

/**
 * Implements a basic solid voxel renderer. Just renders a tile texture with the given light.
 *
 * @author  Christian Sommer
 * @since   6/16/2014
 */
public class DefaultVoxelRenderer implements VoxelRenderer {
    /**
     * Corresponding voxelPatchRenderer
     */
    protected final VoxelPatchRenderer voxelPatchRenderer;

    /**
     * Creates the Voxel Renderer.
     *
     * @param voxelPatchRenderer Corresponding voxelPatchRenderer
     */
    public DefaultVoxelRenderer(VoxelPatchRenderer voxelPatchRenderer) {
        this.voxelPatchRenderer = voxelPatchRenderer;
    }

    @Override
    public void render(GL2 gl, Blitter blitter, VoxelPatch patch, int x, int y, int z, Voxel voxel, int mx, int my) {
        final Map map = voxelPatchRenderer.selectVoxelMap(voxel);
        if (MousePicking.highlights.contains(new VectorI3(x, y, z))) {
            blitter.blit(map, mx, my, z, Color.RED, voxel.index);
        } else {
            blitter.blit(map, mx, my, z, patch.lightPatch.get(x, y, z), voxel.index);
        }
    }
}
