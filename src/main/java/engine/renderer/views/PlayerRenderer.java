/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.views;

import engine.renderer.RendererOptions;
import engine.renderer.blitter.Blitter;
import engine.renderer.blitter.Map;
import engine.renderer.blitter.MapFactory;
import model.Player;
import util.math.VectorF3;
import util.math.VectorI3;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import java.io.File;

/**
 * Renders a player.
 *
 * @author  Christian Sommer
 * @since   1/26/2015
 */
public class PlayerRenderer implements Renderer {
    private final static int VOXEL_SIZE = RendererOptions.get().voxelRenderSize;
    private final Player player;
    private final Map map;

    /**
     * Creates a renderer for the given player model class.
     *
     * @param player player model
     * @param gl GL context
     */
    public PlayerRenderer(Player player, GL2 gl) {
        this.player = player;
        map = MapFactory.loadMapFromFile(gl, new File("player.png"), 1, 1);
    }

    @Override
    public void render(GLAutoDrawable drawable, GL2 gl, Blitter blitter, VectorF3 cameraPosition) {
        final VectorI3 position = player.getPosition();

        blitter.blit(map, position.x * VOXEL_SIZE - (int) cameraPosition.x + ((position.y & 1) == 1 ? VOXEL_SIZE / 2 : 0),
                position.y * VOXEL_SIZE / 4 - (int) cameraPosition.y - 32, position.z, 0xFFFFFFFF, 0);
        blitter.flush(gl);
    }
}
