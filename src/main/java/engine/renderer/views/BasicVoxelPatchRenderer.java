/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.views;

import engine.renderer.RendererOptions;
import engine.renderer.blitter.Blitter;
import model.Voxel;
import model.VoxelPatch;
import util.math.VectorF3;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * BasicVoxelPatchRenderer provides base functionality for rendering a voxel patch.
 * It determines visible voxels and sends them to a render function.
 *
 * @author  Christian Sommer
 * @since   9/5/2014
 */
public abstract class BasicVoxelPatchRenderer {
    private final int VOXEL_SIZE = RendererOptions.get().voxelRenderSize;

    protected GLAutoDrawable drawable;
    protected GL2 gl;
    protected Blitter blitter;
    protected VectorF3 cameraPosition;

    /**
     * Renders a voxel patch.
     *
     * @param drawable GL drawable
     * @param gl GL context
     * @param blitter blitter
     * @param cameraPosition camera position
     * @param patch cell
     * @param wX screen x-offset
     * @param wY screen y-offset
     */
    public void render(GLAutoDrawable drawable, GL2 gl, Blitter blitter, VectorF3 cameraPosition, VoxelPatch patch, int wX, int wY) {
        this.drawable = drawable;
        this.gl = gl;
        this.blitter = blitter;
        this.cameraPosition = cameraPosition;

        final int ox = -wX + (int) cameraPosition.x;
        final int oy = -wY + (int) cameraPosition.y;

        int drawStartWidth = Math.max((ox) / VOXEL_SIZE - 1, 0);
        int drawStartHeight = Math.max((oy) / (VOXEL_SIZE / 4) - 3, 0);
        int drawWidth = Math.min((ox + drawable.getSurfaceWidth()) / VOXEL_SIZE + 1, patch.getWidth());
        int drawHeight = Math.min((oy + drawable.getSurfaceHeight()) / (VOXEL_SIZE / 4) + 1, patch.getHeight());

        startRendering();

        for (int y = drawStartHeight; y < drawHeight; y++) {
            int my = -oy + y * VOXEL_SIZE / 4;
            for (int x = drawStartWidth; x < drawWidth; x++) {
                int mx = -ox + x * VOXEL_SIZE + (y % 2 == 0 ? 0 : VOXEL_SIZE / 2);

                for (int z = patch.getDepth() - 1; z >= 0; z--) {
                    final Voxel voxel = patch.get(x, y, z);
                    if (voxel != null) {
                        final boolean dontStop = renderVoxel(gl, blitter, patch, x, y, z, voxel, mx, my);

                        if (dontStop)
                            break;
                    }
                }
            }
        }

        finishRendering();
    }

    /**
     * Starts Rendering. Do Per-Frame setup stuff here
     */
    protected abstract void startRendering();

    /**
     * Renders a single voxel.
     *
     * @param gl GL context
     * @param blitter blitter
     * @param patch patch
     * @param x x-coordinate
     * @param y x-coordinate
     * @param z x-coordinate
     * @param voxel voxel
     * @param mx screen x-offset
     * @param my screen y-offset
     * @return True if rendered completely .False if there was alpha or something. Render will continue on this position if false
     */
    protected abstract boolean renderVoxel(GL2 gl, Blitter blitter, VoxelPatch patch, int x, int y, int z, Voxel voxel, int mx, int my);

    /**
     * Finishes Rendering. Do Per-Frame cleanup stuff here
     */
    protected abstract void finishRendering();
}
