/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.views;

import engine.renderer.RendererOptions;
import engine.renderer.blitter.Blitter;
import model.Terrain;
import model.VoxelPatch;
import util.math.VectorF3;
import util.math.VectorI2;
import util.math.VectorI3;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * BasicStreamingTerrainRenderer provides base functionality for rendering a streamed terrain.
 * It determines visible cells and sends them to a render function.
 *
 * @author  Christian Sommer
 * @since   9/5/2014
 */
public abstract class BasicStreamingTerrainRenderer implements TerrainRenderer {
    protected final static int VOXEL_SIZE = RendererOptions.get().voxelRenderSize;
    protected final Terrain terrain;
    protected final VectorI2 patchPixelSize;
    protected GLAutoDrawable drawable;
    protected GL2 gl;
    protected Blitter blitter;
    protected VectorF3 cameraPosition;
    protected int startNodeX;
    protected int startNodeY;
    protected int endNodeX;
    protected int endNodeY;

    /**
     * Creates a streaming renderer for the given GL context and terrain model
     *
     * @param gl GL context
     * @param terrain terrain model
     */
    public BasicStreamingTerrainRenderer(GL2 gl, Terrain terrain) {
        this.terrain = terrain;

        patchPixelSize = new VectorI2(
                terrain.getCellSize().x * VOXEL_SIZE,
                terrain.getCellSize().y * VOXEL_SIZE / 4);
    }

    @Override
    public void render(GLAutoDrawable drawable, GL2 gl, Blitter blitter, VectorF3 cameraPosition) {
        this.drawable = drawable;
        this.gl = gl;
        this.blitter = blitter;
        this.cameraPosition = cameraPosition;

        final int startPixelX = (int) cameraPosition.x - VOXEL_SIZE;
        final int endPixelX = (int) cameraPosition.x + drawable.getSurfaceWidth() + VOXEL_SIZE;
        final int startPixelY = (int) cameraPosition.y - VOXEL_SIZE;
        final int endPixelY = (int) cameraPosition.y + drawable.getSurfaceHeight() + VOXEL_SIZE;

        startNodeX = (startPixelX + (startPixelX < 0 ? -patchPixelSize.x : 0)) / patchPixelSize.x;
        startNodeY = (startPixelY + (startPixelY < 0 ? -patchPixelSize.y : 0)) / patchPixelSize.y;
        endNodeX = (endPixelX + (endPixelX < 0 ? -patchPixelSize.x : 0)) / patchPixelSize.x;
        endNodeY = (endPixelY + (endPixelY < 0 ? -patchPixelSize.y : 0)) / patchPixelSize.y;

        startRendering();

        for (int x = startNodeX; x <= endNodeX; x++) {
            for (int y = startNodeY; y <= endNodeY; y++) {
                final VectorI3 nodePos = new VectorI3(x, y, 0);
                final VoxelPatch patch = terrain.getPatch(nodePos);
                renderPatch(patch, nodePos);
            }
        }

        finishRendering();
    }

    /**
     * Starts Rendering. Do setup stuff here (like setting up a blitter).
     */
    protected abstract void startRendering();

    /**
     * Renders a Patch.
     *
     * @param patch Patch to render
     * @param nodePos node position (offset)
     */
    protected abstract void renderPatch(VoxelPatch patch, VectorI3 nodePos);

    /**
     * Finishes Rendering. Clean up per-frame resources (like flushing a blitter).
     */
    protected abstract void finishRendering();
}
