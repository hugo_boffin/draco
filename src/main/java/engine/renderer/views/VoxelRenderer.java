/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.views;

import engine.renderer.blitter.Blitter;
import model.Voxel;
import model.VoxelPatch;

import com.jogamp.opengl.GL2;

/**
 * Base Interface for all Voxel Renderers (Water, grass, ...)
 *
 * @author  Christian Sommer
 * @since   6/16/2014
 */
interface VoxelRenderer {
    /**
     * Renders a concrete voxel.
     *
     * @param gl GL context
     * @param blitter Blitter
     * @param patch cell
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @param voxel Voxel
     * @param mx x-offset
     * @param my y-offset
     */
    void render(GL2 gl, Blitter blitter, VoxelPatch patch, int x, int y, int z, Voxel voxel, int mx, int my);
}
