/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.views;

import engine.renderer.blitter.Blitter;
import model.World;
import util.math.VectorF3;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * WorldRenderer is the Main view class.
 *
 * @author  Christian Sommer
 * @since   6/16/2014
 */
public class WorldRenderer implements Renderer {
    private final World world;
    private final TerrainRenderer terrainRenderer;
    private final TerrainDebugRenderer terrainDebugRenderer;
    private final PlayerRenderer playerRenderer;
    private boolean displayDebugInfo;

    /**
     * Creates a view for the given world model.
     *
     * @param gl GL context
     * @param world world-model
     */
    public WorldRenderer(GL2 gl, World world) {
        displayDebugInfo = false;
        this.world = world;

        this.terrainRenderer = new StreamingTerrainRenderer(gl, world.getTerrain());
        this.terrainDebugRenderer = new StreamingTerrainDebugRenderer(gl, world.getTerrain());

        this.playerRenderer = new PlayerRenderer(this.world.getPlayer(), gl);
    }

    @Override
    public void render(GLAutoDrawable drawable, GL2 gl, Blitter blitter, VectorF3 cameraPosition) {
        terrainRenderer.render(drawable, gl, blitter, cameraPosition);

        if (terrainDebugRenderer != null && displayDebugInfo)
            terrainDebugRenderer.render(drawable, gl, blitter, cameraPosition);

        playerRenderer.render(drawable, gl, blitter, cameraPosition);
    }

    /**
     * True if Debug overlay is activated.
     *
     * @return Debug overlay is activated
     */
    public boolean isDisplayDebugInfo() {
        return displayDebugInfo;
    }

    /**
     * Sets if Debug overlay is activated.
     *
     * @param displayDebugInfo Debug overlay is activated
     */
    public void setDisplayDebugInfo(boolean displayDebugInfo) {
        this.displayDebugInfo = displayDebugInfo;
    }

    /**
     * Returns the current coordinate display.
     *
     * @return coordinate display.
     */
    public TerrainDebugRenderer.CoordinatesDisplay getCoordinatesDisplay() {
        return terrainDebugRenderer.getCoordinatesDisplay();
    }

    /**
     * Sets a specific coordinate display.
     *
     * @param coordinatesDisplay type of display
     */
    public void setCoordinatesDisplay(TerrainDebugRenderer.CoordinatesDisplay coordinatesDisplay) {
        terrainDebugRenderer.setCoordinatesDisplay(coordinatesDisplay);
    }

    /**
     * Rotates coordinate display.
     */
    public void toggleCoordinatesDisplay() {
        terrainDebugRenderer.toggleCoordinatesDisplay();
    }

    /**
     * Returns the number of visible cells
     *
     * @return cell count
     */
    public int getVisibleCellCount() {
        return terrainRenderer.getVisibleCellCount();
    }
}
