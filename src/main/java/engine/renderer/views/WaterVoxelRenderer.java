/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.views;

import engine.renderer.blitter.Blitter;
import engine.renderer.blitter.Map;
import model.Voxel;
import model.VoxelPatch;

import com.jogamp.opengl.GL2;

/**
 * Implements a Water Renderer.
 *
 * Algorithm:
 *      1) Count water voxels under this one.
 *      2) Draw the next "solid" voxel.
 *      3) Depending on the water depth, set alpha and color and render the water voxel.
 *
 * @author  Christian Sommer
 * @since   6/16/2014
 */
public class WaterVoxelRenderer extends DefaultVoxelRenderer {
    /**
     * Creates a water renderer.
     *
     * @param voxelPatchRenderer Corresponding voxelPatchRenderer
     */
    public WaterVoxelRenderer(VoxelPatchRenderer voxelPatchRenderer) {
        super(voxelPatchRenderer);
    }

    @Override
    public void render(GL2 gl, Blitter blitter, VoxelPatch patch, int x, int y, int z, Voxel voxel, int mx, int my) {
        int waterDepth = 0;
        Voxel subVoxel = null;
        int subZ = 0;
        for (int zz = z - 1; zz >= 0; zz--) {
            subVoxel = patch.get(x, y, zz);
            subZ = zz;
            if (subVoxel != null && subVoxel.base == 2) {
                waterDepth++;
                continue;
            }

            if (subVoxel != null && subVoxel.base != 2 || subVoxel == null)
                break;

            if (waterDepth >= 15 || zz <= 0) {
                waterDepth = 15;
                break;
            }
        }

        if (subVoxel != null) {
            VoxelRenderer subRenderer = voxelPatchRenderer.selectVoxelRenderer(subVoxel);
            subRenderer.render(gl, blitter, patch, x, y, subZ, subVoxel, mx, my);
        }

        final int alpha = (int) (64 + 191.0f * ((Math.min(15.0f, (float) waterDepth) / 15.0f)));
        final Map map = voxelPatchRenderer.selectVoxelMap(voxel);
        blitter.blit(map, mx, my, z, patch.lightPatch.get(x, y, z) & ((alpha << 24) | 0xFFFFFF), voxel.index);
    }
}
