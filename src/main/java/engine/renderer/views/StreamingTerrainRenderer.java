/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.views;

import engine.renderer.RendererOptions;
import engine.renderer.blitter.Map;
import engine.renderer.blitter.MapFactory;
import model.Terrain;
import model.VoxelPatch;
import util.math.VectorI3;
import util.tiles.TileGenerator;

import com.jogamp.opengl.GL2;
import java.awt.*;
import java.util.ArrayList;

/**
 * Implements a streaming terrain renderer.
 *
 * @author  Christian Sommer
 * @since   7/5/2014
 */
public class StreamingTerrainRenderer extends BasicStreamingTerrainRenderer {
    private final TileGenerator tg = new TileGenerator(VOXEL_SIZE, false, false, true, Color.WHITE, false);
    private final ArrayList<Map> maps = new ArrayList<>();
    private final VoxelPatchRenderer patchRenderer;
    private final int terrainCellWarmupRadius = RendererOptions.get().terrainCellWarmupRadius;

    private int visibleCells;

    /**
     * Creates a StreamingTerrainRenderer for the given terrain model.
     *
     * @param gl GL context
     * @param terrain terrain model
     */
    public StreamingTerrainRenderer(GL2 gl, Terrain terrain) {
        super(gl, terrain);

        visibleCells = 0;

        terrain.getVoxelTypes().forEach(
                (g) -> maps.add(MapFactory.createMapFromBufferedImage(gl, tg.renderTile(g), 4, 4)));

        patchRenderer = new VoxelPatchRenderer(terrain.getVoxelTypes(), maps);
        patchRenderer.setAutoFlush(false);
    }

    @Override
    protected void startRendering() {
        visibleCells = 0;
    }

    @Override
    protected void renderPatch(VoxelPatch patch, VectorI3 nodePos) {
        patchRenderer.render(drawable, gl, blitter, cameraPosition, patch, nodePos.x * patchPixelSize.x, nodePos.y * patchPixelSize.y);
        visibleCells++;
    }

    @Override
    protected void finishRendering() {
        patchRenderer.flush(gl, blitter);

        for (int x = startNodeX - terrainCellWarmupRadius; x <= endNodeX + terrainCellWarmupRadius; x++)
            for (int y = startNodeY - terrainCellWarmupRadius; y <= endNodeY + terrainCellWarmupRadius; y++)
                terrain.warmupPatch(new VectorI3(x, y, 0));
    }

    @Override
    public int getVisibleCellCount() {
        return visibleCells;
    }
}
