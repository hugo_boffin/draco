/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer;

import engine.renderer.blitter.BlitterType;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

/**
 * Singleton class for Game (Renderer) Options.
 *
 * @author  Christian Sommer
 * @since   5/13/2014
 */
public class RendererOptions {
    private static final Logger logger = LogManager.getLogger(RendererOptions.class.getName());
    private static RendererOptions renderOptions = null;

    public int screenWidth = 1680;
    public int screenHeight = 1050;
    public boolean debugGL = false;
    public int voxelRenderSize = 32;
    public boolean limitFPS = true;
    public int maxFPS = 60;
    public BlitterType blitterType = BlitterType.VBO;
    public int terrainCellWarmupRadius = 1;
    public boolean waitForVerticalSync = false;

    /**
     * returns the singleton object. Tries to load the options from a groovy script. create a default one otherwise.
     *
     * @return World Instance
     */
    public static RendererOptions get() {
        if (renderOptions == null) {
            final File configFile = new File("configuration.groovy");
            RendererOptions newRenderOptions = new RendererOptions();

            try {
                Binding binding = new Binding();
                binding.setVariable("rendererOptions", newRenderOptions);
                GroovyShell shell = new GroovyShell(binding);
                String[] list = new String[0];
                newRenderOptions = (RendererOptions) shell.run(configFile, list);
            } catch (IOException e) {
                logger.info("Unable to load config file. Using default configuration ...");
                newRenderOptions = new RendererOptions();
            }

            renderOptions = newRenderOptions;
        }

        return renderOptions;
    }
}
