/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.blitter;

import com.jogamp.opengl.util.awt.ImageUtil;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;
import engine.Loader;

import javax.imageio.ImageIO;
import com.jogamp.opengl.GL2;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Map Factory.
 *
 * @author  Christian Sommer
 * @since   3/3/2015.
 */
public class MapFactory {
    private static Texture notFoundTexture = null;

    /**
     * Returns a default not-found texture
     *
     * @param gl GL context
     * @return not-found texture
     */
    private static Texture getNotFoundTexture(GL2 gl) {
        if (notFoundTexture == null) {
            BufferedImage bi = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
            bi.getGraphics().clearRect(0, 0, 32, 32);
            bi.getGraphics().setColor(Color.WHITE);
            bi.getGraphics().fillRect(0, 0, 16, 16);
            bi.getGraphics().fillRect(16, 16, 16, 16);
            final TextureData td = AWTTextureIO.newTextureData(gl.getGLProfile(), bi, false);
            notFoundTexture = TextureIO.newTexture(td);
        }
        return notFoundTexture;
    }

    /**
     * Creates a texture map from a BufferedImage
     *
     * @param gl GL context
     * @param bufferedImage buffered image
     * @param tx Number of Tiles in x-direction
     * @param ty Number of Tiles in y-direction
     * @return created map
     */
    public static Map createMapFromBufferedImage(GL2 gl, BufferedImage bufferedImage, int tx, int ty) {
        ImageUtil.flipImageVertically(bufferedImage);
        final TextureData td = AWTTextureIO.newTextureData(gl.getGLProfile(), bufferedImage, false);
        return new Map(gl, TextureIO.newTexture(td), tx, ty);
    }

    /**
     * Loads a texture map from file.
     *
     * @param gl GL context
     * @param fileName resource filename
     * @param tx Number of Tiles in x-direction
     * @param ty Number of Tiles in y-direction
     * @return created map
     */
    public static Map loadMapFromFile(GL2 gl, File fileName, int tx, int ty) {
        Texture tex;

        InputStream inputStream = Loader.get().getResource(fileName.toString());

        if (inputStream == null) {
            tex = getNotFoundTexture(gl);
        } else {
            try {
                final BufferedImage img = ImageIO.read(inputStream);
                ImageUtil.flipImageVertically(img);
                final TextureData td = AWTTextureIO.newTextureData(gl.getGLProfile(), img, false);
                tex = TextureIO.newTexture(td);
            } catch (IOException e) {
                tex = getNotFoundTexture(gl);
            }
        }

        return new Map(gl, tex, tx, ty);
    }
}
