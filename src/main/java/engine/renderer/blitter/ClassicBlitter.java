/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.blitter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import java.awt.*;
import java.util.Comparator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Simple Blitter Blitting Implementation through OpenGL glBegin and -End Calls.
 *
 * @author  Christian Sommer
 * @since   9/10/2013
 */
public class ClassicBlitter implements Blitter {
    private static final Logger logger = LogManager.getLogger(ClassicBlitter.class.getName());
    private Map activeMap = null;
    private final int maxBlitCount = 25000;
    private final Blit[] blitBuffer = new Blit[maxBlitCount];
    private int blitCount = 0;
    private int count = 0;

    /**
     * Creates a classic blitter
     *
     * @param gl GL context
     */
    public ClassicBlitter(GL2 gl) {
        for (int i = 0; i < maxBlitCount; i++)
            blitBuffer[i] = new Blit();
    }

    @Override
    public void blit(Map map, int x, int y, int z, Color color, int subIndex) {
        blit(map, x, y, z, color.getRGB(), subIndex);
    }

    @Override
    public void blit(Map map, int x, int y, int color, int subIndex) {
        blit(map, x, y, 0, color, subIndex);
    }

    @Override
    public void blit(Map map, int x, int y, int z, int color, int subIndex) {
        if (map == null)
            return;

        if (blitCount == maxBlitCount) {
            logger.error("Classic Blitter Buffer Overflow ... ");
        }

        final float sx = subIndex % map.getTw();
        final float sy = map.getTh() - 1 - subIndex / map.getTw();

        final Blit blit = blitBuffer[blitCount];
        blit.map = map;
        blit.color = color;

        blit.uv_x = map.getFw() * sx;
        blit.uv_y = map.getFh() * (sy + 1);
        blit.uv_z = map.getFw() * (sx + 1);
        blit.uv_w = map.getFh() * sy;

        blit.pos_x = x;
        blit.pos_y = y;
        blit.pos_z = x + map.getW();
        blit.pos_w = y + map.getH();

        blit.depthValue = z;

        blitCount++;
        count++;
    }

    @Override
    public void blit(Map map, int x, int y, Color color, int subIndex) {
        blit(map, x, y, 0, color.getRGB(), subIndex);
    }

    @Override
    public void flush(GL2 gl) {
        flush(gl, null);
    }

    @Override
    public void flush(GL2 gl, Comparator<Blit> comparator) {
        if (blitCount == 0)
            return;

        gl.glEnable(GL.GL_TEXTURE_2D);
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        Stream<Blit> blitStream = IntStream.range(0, blitCount).mapToObj((i) -> blitBuffer[i]);

        if (comparator != null)
            blitStream = blitStream.sorted(comparator);

        activeMap = null;
        blitStream.forEach(blit -> {
            if (activeMap != blit.map) {
                if (activeMap != null) {
                    gl.glEnd();
                }
                blit.map.getTexture().bind(gl);
                activeMap = blit.map;
                gl.glBegin(GL2.GL_QUADS);
            }

            gl.glColor4ub((byte) ((blit.color) & 0xFF),
                    (byte) ((blit.color >> 8) & 0xFF),
                    (byte) ((blit.color >> 16) & 0xFF),
                    (byte) ((blit.color >> 24) & 0xFF)
            );

            gl.glTexCoord2f(blit.uv_x, blit.uv_y);
            gl.glVertex2f(blit.pos_x, blit.pos_y);
            gl.glTexCoord2f(blit.uv_x, blit.uv_w);
            gl.glVertex2f(blit.pos_x, blit.pos_w);
            gl.glTexCoord2f(blit.uv_z, blit.uv_w);
            gl.glVertex2f(blit.pos_z, blit.pos_w);
            gl.glTexCoord2f(blit.uv_z, blit.uv_y);
            gl.glVertex2f(blit.pos_z, blit.pos_y);
        });
        gl.glEnd();

        gl.glDisable(GL.GL_TEXTURE_2D);
        gl.glDisable(GL.GL_BLEND);

        blitCount = 0;
    }

    @Override
    public void resetBlitCounter() {
        count = 0;
    }

    @Override
    public int getBlitCount() {
        return count;
    }
}
