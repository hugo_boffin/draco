/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.blitter;

import com.jogamp.opengl.GL2;
import java.awt.*;
import java.util.Comparator;

/**
 * Interface for a concrete (OpenGL) Blitter.
 *
 * @author  Christian Sommer
 * @since   3/4/2013 12:16 PM
 */
public interface Blitter {
    /**
     * Blits an image.
     *  @param map       texture
     * @param x         x-coordinate
     * @param y         y-coordinate
     * @param color     color
     * @param subIndex  tile index
     */
    void blit(Map map, int x, int y, Color color, int subIndex);

    /**
     * Blits an image with depth.
     *  @param map       texture
     * @param x         x-coordinate
     * @param y         y-coordinate
     * @param z         z-coordinate
     * @param color     color
     * @param subIndex  tile index
     */
    void blit(Map map, int x, int y, int z, Color color, int subIndex);

    /**
     * Blits an image.
     *  @param map       texture
     * @param x         x-coordinate
     * @param y         y-coordinate
     * @param color     color
     * @param subIndex  tile index
     */
    void blit(Map map, int x, int y, int color, int subIndex);

    /**
     * Blits an image with depth.
     *  @param map       texture
     * @param x         x-coordinate
     * @param y         y-coordinate
     * @param z         z-coordinate
     * @param color     color
     * @param subIndex  tile index
     */
    void blit(Map map, int x, int y, int z, int color, int subIndex);

    /**
     * Flushes all buffered blits and sends them to the GL pipeline. Output will be ordererd.
     *
     * @param gl GL context
     * @param comparator Comparator
     */
    void flush(GL2 gl, Comparator<Blit> comparator);

    /**
     * Flushes all buffered blits and sends them to the GL pipeline.
     *
     * @param gl GL context
     */
    void flush(GL2 gl);

    /**
     * Sets the blit count to zero.
     */
    void resetBlitCounter();

    /**
     * Returns number of blits.
     *
     * @return number of blits
     */
    int getBlitCount();
}
