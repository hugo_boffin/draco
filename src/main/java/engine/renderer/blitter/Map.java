/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.blitter;

import com.jogamp.opengl.util.texture.Texture;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

/**
 * Map provides Texture and Tiling Support for Blitting.
 *
 * @author  Christian Sommer
 * @since   9/11/2013
 */
public class Map {
    private final Texture texture;
    private final int w;
    private final int h;
    private final int tw;
    private final int th;
    private final float fw;
    private final float fh;

    /**
     * Creates a GL texture with the given subtiles.
     *
     * @param gl GL Context
     * @param texture texture
     * @param tx Number of Tiles in x-direction
     * @param ty Number of Tiles in y-direction
     */
    public Map(GL2 gl, Texture texture, int tx, int ty) {
        this.texture = texture;

        texture.bind(gl);
        texture.enable(gl);
        texture.setTexParameteri(gl, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
        texture.setTexParameteri(gl, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
        texture.setTexParameteri(gl, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
        texture.setTexParameteri(gl, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);

        w = (texture.getWidth() / tx);
        h = (texture.getHeight() / ty);
        tw = (tx);
        th = (ty);

        fw = (1f / (float) tx);
        fh = (1f / (float) ty);
    }

    /**
     * Returns the texture.
     *
     * @return texture
     */
    public Texture getTexture() {
        return texture;
    }

    /**
     * Returns the tile width.
     *
     * @return tile width
     */
    public int getW() {
        return w;
    }

    /**
     * Returns the tile height.
     *
     * @return tile height
     */
    public int getH() {
        return h;
    }

    /**
     * Returns the entire texture width.
     *
     * @return texture width
     */
    public int getTw() {
        return tw;
    }

    /**
     * Returns the entire texture height.
     *
     * @return texture height
     */
    public int getTh() {
        return th;
    }

    /**
     * Returns the tile width in texture space [0..1]
     *
     * @return tile width in texture space [0..1]
     */
    public float getFw() {
        return fw;
    }

    /**
     * Returns the tile height in texture space [0..1]
     *
     * @return tile height in texture space [0..1]
     */
    public float getFh() {
        return fh;
    }
}