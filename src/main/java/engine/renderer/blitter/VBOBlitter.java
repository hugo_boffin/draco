/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.blitter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import java.awt.*;
import java.nio.ByteBuffer;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * More optimized Blitter implementation. Utilizes vertex buffer objects.
 *
 * @author  Christian Sommer
 * @since   9/10/2013
*/
public class VBOBlitter implements Blitter {
    private static final Logger logger = LogManager.getLogger(VBOBlitter.class.getName());

    private final int maxBlitCount = 25000;
    private final int vertexSize = 4 + 4 * 3 + 4 * 2;
    private final Blit[] blitBuffer = new Blit[maxBlitCount];
    private int blitCount = 0;
    private int vboId = -1;
    private int count = 0;

    /**
     * Creates a VBO blitter
     *
     * @param gl GL context
     */
    public VBOBlitter(GL2 gl) {
        for (int i = 0; i < maxBlitCount; i++)
            blitBuffer[i] = new Blit();

        int[] tmp = new int[1];
        gl.glGenBuffers(1, tmp, 0);
        vboId = tmp[0];
        if (vboId < 1)
            System.err.println("Error getting VBO id");

        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboId);
        gl.glBufferData(GL.GL_ARRAY_BUFFER, (long) maxBlitCount * 4 * vertexSize, null, GL.GL_DYNAMIC_DRAW);
        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
    }

    @Override
    public void blit(Map map, int x, int y, int z, Color color, int subIndex) {
        blit(map, x, y, z, color.getRGB(), subIndex);
    }

    @Override
    public void blit(Map map, int x, int y, int color, int subIndex) {
        blit(map, x, y, 0, color, subIndex);
    }

    @Override
    public void blit(Map map, int x, int y, int z, int color, int subIndex) {
        if (map == null)
            return;
        if (blitCount == maxBlitCount) {
            logger.error("VBO Buffer Overflow ... ");
        }

        final float sx = subIndex % map.getTw();
        final float sy = map.getTh() - 1 - subIndex / map.getTw();

        final Blit blit = blitBuffer[blitCount];
        blit.map = map;
        blit.color = color;

        blit.uv_x = map.getFw() * sx;
        blit.uv_y = map.getFh() * (sy + 1);
        blit.uv_z = map.getFw() * (sx + 1);
        blit.uv_w = map.getFh() * sy;

        blit.pos_x = x;
        blit.pos_y = y;
        blit.pos_z = x + map.getW();
        blit.pos_w = y + map.getH();

        blit.depthValue = z;

        blitCount++;
        count++;
    }

    @Override
    public void blit(Map map, int x, int y, Color color, int subIndex) {
        blit(map, x, y, 0, color.getRGB(), subIndex);
    }

    @Override
    public void flush(GL2 gl) {
        flush(gl, null);
    }

    @Override
    public void flush(GL2 gl, Comparator<Blit> comparator) {
        class DrawBatch {
            public final int offset;
            public final int blitCount;
            public final Map map;

            DrawBatch(int offset, int blitCount, Map map) {
                this.offset = offset;
                this.blitCount = blitCount;
                this.map = map;
            }
        }

        class MappingTemp {
            int i = 0;
            int offset = 0;
            Map activeMap = null;
            int blitCount = 0;
            final LinkedList<DrawBatch> batches = new LinkedList<>();

            public void advance(Blit blit) {
                if (activeMap != blit.map) {
                    flush();
                    activeMap = blit.map;
                    offset += blitCount;
                    blitCount = 0;
                }
                blitCount++;
                i++;
            }

            public void flush() {
                if (blitCount != 0 && activeMap != null) {
                    batches.add(new DrawBatch(offset, blitCount, activeMap));
                }
            }
        }

        if (blitCount == 0)
            return;


        gl.glEnable(GL.GL_TEXTURE_2D);
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboId);
        gl.glBufferData(GL.GL_ARRAY_BUFFER, (long) blitCount * 4 * vertexSize, null, GL.GL_DYNAMIC_DRAW);
        // Null Pointer on Data attribute, this tells the driver to flush everything und does not stall rendering pipeline
        // as descriped in http://developer.download.nvidia.com/assets/gamedev/docs/Using-VBOs.pdf

        ByteBuffer buffer = gl.glMapBuffer(GL.GL_ARRAY_BUFFER, GL2.GL_WRITE_ONLY);

        Stream<Blit> blitStream = IntStream.range(0, blitCount).mapToObj((i) -> blitBuffer[i]);

        if (comparator != null)
            blitStream = blitStream.sorted(comparator);

        final MappingTemp t = new MappingTemp();
        blitStream.forEach(blit -> {
            buffer.putInt(blit.color);
            buffer.putFloat(blit.uv_x);
            buffer.putFloat(blit.uv_y);
            buffer.putFloat(blit.pos_x);
            buffer.putFloat(blit.pos_y);
            buffer.putFloat(0.0f);

            buffer.putInt(blit.color);
            buffer.putFloat(blit.uv_x);
            buffer.putFloat(blit.uv_w);
            buffer.putFloat(blit.pos_x);
            buffer.putFloat(blit.pos_w);
            buffer.putFloat(0.0f);

            buffer.putInt(blit.color);
            buffer.putFloat(blit.uv_z);
            buffer.putFloat(blit.uv_w);
            buffer.putFloat(blit.pos_z);
            buffer.putFloat(blit.pos_w);
            buffer.putFloat(0.0f);

            buffer.putInt(blit.color);
            buffer.putFloat(blit.uv_z);
            buffer.putFloat(blit.uv_y);
            buffer.putFloat(blit.pos_z);
            buffer.putFloat(blit.pos_y);
            buffer.putFloat(0.0f);

            t.advance(blit);
        });
        t.flush();

        gl.glUnmapBuffer(GL.GL_ARRAY_BUFFER);
        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);

        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboId);

        gl.glEnableClientState(GL2.GL_COLOR_ARRAY);
        gl.glEnableClientState(GL2.GL_TEXTURE_COORD_ARRAY);
        gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);

        gl.glColorPointer(4, GL.GL_UNSIGNED_BYTE, vertexSize, 0);
        gl.glTexCoordPointer(2, GL.GL_FLOAT, vertexSize, 4);
        gl.glVertexPointer(3, GL.GL_FLOAT, vertexSize, 4 + 4 * 2);

        t.batches.forEach(batch -> {
            batch.map.getTexture().bind(gl);
            gl.glDrawArrays(GL2.GL_QUADS, batch.offset * 4, batch.blitCount * 4);
        });

        gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL2.GL_TEXTURE_COORD_ARRAY);
        gl.glDisableClientState(GL2.GL_COLOR_ARRAY);

        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);


        gl.glDisable(GL.GL_TEXTURE_2D);
        gl.glDisable(GL.GL_BLEND);

        blitCount = 0;
    }

    @Override
    public void resetBlitCounter() {
        count = 0;
    }

    @Override
    public int getBlitCount() {
        return count;
    }
}
