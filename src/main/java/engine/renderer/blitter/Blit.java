/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.blitter;

import java.awt.*;

/**
 * A Blit describes on painting of an image to the surface.
 * Everything in here is public and immutable due to the blitter architecture.
 * This is not so problematic as blit will never be used outside of this package.
 *
 * @author  Christian Sommer
 * @since   6/13/2014
 */
public class Blit {
    /**
     * Texture Map to use
     */
    public Map map;

    /**
     * Blit Color
     */
    public int color;

    /**
     * Texture Coordinates
     */
    public float uv_x, uv_y, uv_z, uv_w;

    /**
     * Surface Coordinates
     */
    public float pos_x, pos_y, pos_z, pos_w;

    /**
     * Depth Value
     */
    public float depthValue;

    /**
     * Creates an empty blit.
     */
    public Blit() {
        map = null;
        color = Color.WHITE.getRGB();
        depthValue = 0.0f;
        uv_x = uv_y = uv_z = uv_w = pos_x = pos_y = pos_z = 0.0f;
        pos_w = 1.0f;
    }
}
