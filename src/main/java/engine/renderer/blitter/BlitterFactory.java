/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.blitter;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jogamp.opengl.GL2;

/**
 * Blitter Factory.
 *
 * @author  Christian Sommer
 * @since   2/24/2015
 */
public class BlitterFactory {
    private static final Logger logger = LogManager.getLogger(BlitterFactory.class.getName());

    /**
     * Factory Method creates a concrete blitter.
     *
     * @param type type of requested blitter
     * @param gl GL context
     * @return concrete blitter
     */
    public static Blitter create(BlitterType type, GL2 gl) {
        Blitter blitter = null;
        switch (type) {
            case Classic -> blitter = new ClassicBlitter(gl);
            case VBO -> blitter = new VBOBlitter(gl);
            case Null -> blitter = new NullBlitter();
            default -> {
                logger.warn("Unable to determine request blitter. Creating a null blitter instead...");
                blitter = new NullBlitter();
            }
        }

        logger.log(Level.INFO, "Created " + blitter.getClass().getSimpleName() + "...");
        return blitter;
    }
}
