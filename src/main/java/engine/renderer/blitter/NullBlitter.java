/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.renderer.blitter;

import com.jogamp.opengl.GL2;
import java.awt.*;
import java.util.Comparator;

/**
 * Implements a Null Blitter. This does not render anything.
 *
 * @author  Christian Sommer
 * @since   9/10/2013 12:21 PM
 */
public class NullBlitter implements Blitter {
    @Override
    public void blit(Map map, int x, int y, Color color, int subIndex) {
    }

    @Override
    public void blit(Map map, int x, int y, int z, Color color, int subIndex) {
    }

    @Override
    public void blit(Map map, int x, int y, int color, int subIndex) {
    }

    @Override
    public void blit(Map map, int x, int y, int z, int color, int subIndex) {
    }

    @Override
    public void flush(GL2 gl, Comparator<Blit> comparator) {
    }

    @Override
    public void flush(GL2 gl) {
    }

    @Override
    public void resetBlitCounter() {
    }

    @Override
    public int getBlitCount() {
        return 0;
    }
}
