/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.input;

import com.jogamp.newt.event.KeyEvent;

/**
 * A concrete switch-like KeyAction. Handy for e.g. moving keys.
 *
 * @author  Christian Sommer
 * @since   3/4/2015
 */
public class StateKeyAction implements KeyAction {
    private boolean state = false;

    /**
     * Creates a StateKeyAction
     */
    public StateKeyAction() {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        state = true;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        state = false;
    }

    /**
     * Returns true if the button is still pressed.
     *
     * @return true if pressed
     */
    public boolean isPressed() {
        return state;
    }
}
