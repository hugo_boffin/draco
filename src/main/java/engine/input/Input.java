/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.input;

import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.KeyListener;
import com.jogamp.newt.event.MouseEvent;
import com.jogamp.newt.event.MouseListener;
import com.jogamp.newt.opengl.GLWindow;

import java.util.HashMap;

/**
 * Main class for input device handling. Stores key events in a hashmap and fires
 * them if the corresponding keys are pressed/released.
 *
 * @author  Christian Sommer
 * @since   3/3/2015
 */
public class Input {
    private final GLWindow glWindow;
    private final HashMap<Short, KeyAction> keyMap = new HashMap<>();
    private final KeyListener keyListener = new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
            KeyAction action = keyMap.get(e.getKeyCode());
            if (action != null)
                action.keyPressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.isAutoRepeat())
                return;

            KeyAction action = keyMap.get(e.getKeyCode());
            if (action != null)
                action.keyReleased(e);
        }
    };
    private int mouseX = 0, mouseY = 0;
    private final MouseListener mouseListener = new MouseListener() {
        @Override
        public void mouseClicked(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {

        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseMoved(MouseEvent mouseEvent) {
            mouseX = mouseEvent.getX();
            mouseY = mouseEvent.getY();
        }

        @Override
        public void mouseDragged(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseWheelMoved(MouseEvent mouseEvent) {

        }
    };

    public Input(GLWindow glWindow) {
        this.glWindow = glWindow;
        glWindow.addKeyListener(keyListener);
        glWindow.addMouseListener(mouseListener);
    }

    /**
     * Returns the mouse x-coordinate
     *
     * @return mouse x-coordinate
     */
    public int getMouseX() {
        return mouseX;
    }

    /**
     * Returns the mouse' y-coordinate
     *
     * @return mouse' y-coordinate
     */
    public int getMouseY() {
        return mouseY;
    }

    /**
     * Adds a new keyAction to the given key.
     *
     * @param key key code
     * @param keyAction key action
     */
    public void addKeyAction(short key, KeyAction keyAction) {
        keyMap.put(key, keyAction);
    }
}
