/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.input;

import com.jogamp.newt.event.KeyEvent;

/**
 * A concrete KeyAction which triggers another action on release (e.g. when opening a menu, or firing an hotbar skill).
 *
 * @author  Christian Sommer
 * @since   3/4/2015
 */
public class TriggerKeyAction implements KeyAction {
    private final Action action;

    /**
     * Creates a TriggerKeyAction with the given action.
     *
     * @param action action to trigger
     */
    public TriggerKeyAction(Action action) {
        this.action = action;
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        action.trigger(e);
    }

    /**
     * Interface for an action.
     */
    public interface Action {
        /**
         * Triggered when key is released.
         *
         * @param e KeyEvent
         */
        void trigger(KeyEvent e);
    }
}
