/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package engine.input;

import com.jogamp.newt.event.KeyEvent;

/**
 * Interface for keyboard key event.
 *
 * @author  Christian Sommer
 * @since   3/4/2015
 */
public interface KeyAction {
    /**
     * Method will be fired if the chosen key is pressed.
     *
     * @param e KeyEvent
     */
    void keyPressed(KeyEvent e);

    /**
     * Method will be fired if the chosen key is released.
     *
     * @param e KeyEvent
     */
    void keyReleased(KeyEvent e);
}
