/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util.math;

/**
 * This class implements an immutable Rectangle describing a range in I^2.
 *
 * @author  Christian Sommer
 * @since   9/5/2014
 */
public class Rectangle {
    /**
     * left boundary
     */
    private final int left;

    /**
     * right boundary
     */
    private final int right;

    /**
     * top boundary
     */
    private final int top;

    /**
     * bottom boundary
     */
    private final int bottom;

    /**
     * Constructor which sets all values.
     *
     * @param left left boundary
     * @param right right boundary
     * @param top top boundary
     * @param bottom bottom boundary
     */
    public Rectangle(int left, int right, int top, int bottom) {
        if (left < right) {
            this.left = left;
            this.right = right;
        } else {
            this.right = left;
            this.left = right;
        }
        if (top < bottom) {
            this.top = top;
            this.bottom = bottom;
        } else {
            this.bottom = top;
            this.top = bottom;
        }
    }

    /**
     * Returns left boundary
     *
     * @return left boundary
     */
    public int getLeft() {
        return left;
    }

    /**
     * Returns right boundary
     *
     * @return right boundary
     */
    public int getRight() {
        return right;
    }

    /**
     * Returns top boundary
     *
     * @return top boundary
     */
    public int getTop() {
        return top;
    }

    /**
     * Returns bottom boundary
     *
     * @return bottom boundary
     */
    public int getBottom() {
        return bottom;
    }

    /**
     * Returns the width of the rectangle
     *
     * @return rectangle width
     */
    public int getWidth() {
        return right - left;
    }

    /**
     * Returns the height of the rectangle
     *
     * @return rectangle height
     */
    public int getHeight() {
        return bottom - top;
    }
}
