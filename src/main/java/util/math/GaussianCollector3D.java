/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util.math;

/**
 * Gaussian Function working in diamond map mode. Implemented using a 3-pass algorithm.
 * Pass in a Collector Function. This algorithm requires your patch to overlap
 * (depending on the gauss radius).
 *
 * @author  Christian Sommer
 * @since   3/7/2015
 */
public class GaussianCollector3D {
    /**
     * Generic Collector Function
     *
     * @param <U> aggregation type
     * @param <V> Array3D value type
     */
    public interface CollectorFunction<U, V> {
        U collect(U old, V voxel);
    }

    private final int d;
    private final boolean negativeZ;
    private final Rectangle overlapping;

    /**
     * Creates a Gaussian Collector.
     *
     * @param radius gauss radius
     * @param negativeZ if true, z axis evaluates [-d,d], [0,d] otherwise
     */
    public GaussianCollector3D(int radius, boolean negativeZ) {
        this.d = radius;
        this.negativeZ = negativeZ;
        this.overlapping = new Rectangle(d*2, d*2, d*4, d*4);
    }

    /**
     * Computes the gaussian 3-pass algorithm and returns the aggregated array.
     *
     * @param patch source array3d
     * @param initVal start value for aggregation/folding
     * @param nullVal value for null source values.
     * @param initCollFunc aggregation function ford first pass
     * @param collFunc aggregation function for 2nd and 3rd pass
     * @param <U> aggregation type
     * @param <V> array3d source value type
     * @return aggregated array3d
     */
    public <U, V> Array3D<U> compute(Array3D<V> patch, U initVal, U nullVal,
                                     CollectorFunction<U,V> initCollFunc, CollectorFunction<U,U> collFunc) {
        final int occlusionRadius = d;

        Array3D<U> xPass = new Array3D<>(patch.getWidth(),patch.getHeight(),patch.getDepth(), (x,y,z) ->
        {
            if (x < occlusionRadius || x >= patch.getWidth() - occlusionRadius ||
                    y < occlusionRadius || y >= patch.getHeight() - occlusionRadius)
                return nullVal;

            if (patch.get(x, y, z) == null)
                return nullVal;

            U fillCount = initVal;
            int ox = x - d / 2 - ((y % 2 == 0)?1:0);
            int oy = y + d;

            for (int o = 0; o <= 2*d; o++) {
                fillCount = initCollFunc.collect(fillCount, patch.get(ox, oy, z));

                if (oy % 2 != 0)
                    ox++;
                oy--;
            }

            return fillCount;
        });

        Array3D<U> yPass = new Array3D<>(patch.getWidth(),patch.getHeight(),patch.getDepth(), (x,y,z) ->
        {
            if (x < occlusionRadius || x >= patch.getWidth() - occlusionRadius ||
                    y < occlusionRadius || y >= patch.getHeight() - occlusionRadius)
                return nullVal;

            if (patch.get(x, y, z) == null)
                return nullVal;

            U fillCount = initVal;
            int ox = x + d / 2 + ((y % 2 != 0)?1:0);
            int oy = y + d;

            for (int o = 0; o <= 2*d; o++) {
                fillCount = collFunc.collect(fillCount, xPass.get(ox, oy, z));

                if (oy % 2 == 0)
                    ox--;
                oy--;
            }

            return fillCount;
        });

        Array3D<U> zPass =  new Array3D<>(patch.getWidth(),patch.getHeight(),patch.getDepth(), (x,y,z) ->
        {
            if (x < 0 || x >= patch.getWidth() ||
                    y < occlusionRadius*2 || y >= patch.getHeight() - (negativeZ ? occlusionRadius*2 : 0))
                return nullVal;

            if (patch.get(x, y, z) == null)
                return nullVal;

            U fillCount = initVal;

            for (int o = (negativeZ ? -d : 0); o <= d; o++)
                if (z+o <= patch.getDepth() && z+o >= 0)
                    fillCount = collFunc.collect(fillCount, yPass.get(x, y-2*o, z+o));

            return fillCount;
        });

        return zPass;
    }

    /**
     * Returns the radius set.
     *
     * @return gauss radius
     */
    public int getRadius() {
        return d;
    }

    /**
     * Returns the maximum possible cell probes per value type (e.g. voxel).
     *
     * @return maximum possible cell probes
     */
    public int getCellCount() {
        if (negativeZ)
            return (2*d+1)*(2*d+1)*(2*d+1);
        else
            return (2*d+1)*(2*d+1)*(d+1);
    }

    /**
     * Returns the required overlapping
     *
     * @return required overlapping
     */
    public Rectangle getOverlapping() {
        return overlapping;
    }
}
