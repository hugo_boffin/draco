/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util.math;

/**
 * This class implements an immutable, comparable vector in R^3 space.
 *
 * @author  Christian Sommer
 * @since   6/13/2014
 */
public class VectorF3 implements Comparable<VectorF3> {
    /**
     * Null Vector
     */
    public static final VectorF3 NULL = new VectorF3(0, 0, 0);

    /**
     * Immutable vector values(coordinates).
     */
    public final double x, y, z;

    /**
     * Basic vector constructor. Since this class is immutable, all values need to be set.
     *
     * @param x x-value
     * @param y y-value
     * @param z z-value
     */
    public VectorF3(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Adds two vectors and returns the sum.
     *
     * @param a summand 1
     * @param b summand 2
     * @return Sum of the two vectors.
     */
    public static VectorF3 add(VectorF3 a, VectorF3 b) {
        return new VectorF3(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    /**
     * Subtracts two vectors and returns the difference.
     *
     * @param a minuend
     * @param b subtrahend
     * @return difference of the two vectors.
     */
    public static VectorF3 sub(VectorF3 a, VectorF3 b) {
        return new VectorF3(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    /**
     * Multiplies two vectors and returns the product.
     *
     * @param a factor 1
     * @param b factor 2
     * @return product of the two vectors.
     */
    public static VectorF3 mul(VectorF3 a, VectorF3 b) {
        return new VectorF3(a.x * b.x, a.y * b.y, a.z * b.z);
    }

    /**
     * Multiplies one vector with a scalar value and returns the product.
     *
     * @param a factor 1
     * @param b factor 2
     * @return product of the vector and the scalar value
     */
    public static VectorF3 mul(VectorF3 a, double b) {
        return new VectorF3(a.x * b, a.y * b, a.z * b);
    }

    /**
     * Normalizes a vector (scale a vector to length = 1).
     *
     * @param v vector to normalize
     * @return normalized vector
     */
    public static VectorF3 normalize(VectorF3 v) {
        double mag = Math.sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
        if (mag == 0)
            return NULL;
        return new VectorF3(v.x / mag, v.y / mag, v.z / mag);
    }

    /**
     * Calculates the dot product of two vectors.
     *
     * @param a vector 1
     * @param b vector 2
     * @return dot product of the two vectors
     */
    public static double dot(VectorF3 a, VectorF3 b) {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    /**
     * Calculates the squared distance between two vectors.
     *
     * @param a vector 1
     * @param b vector 2
     * @return the squared distance between the two vectors
     */
    public static double distSqr(VectorF3 a, VectorF3 b) {
        final VectorF3 dif = sub(b, a);
        return dot(dif, dif);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VectorF3 vectorF3 = (VectorF3) o;

        if (Double.compare(vectorF3.x, x) != 0) return false;
        if (Double.compare(vectorF3.y, y) != 0) return false;
        if (Double.compare(vectorF3.z, z) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public int compareTo(VectorF3 o) {
        return (x == o.x && y == o.y && z == o.z) ? 0 : 1;
    }

    @Override
    public String toString() {
        return "(" +
                "" + String.format("%.2f", x) +
                "," + String.format("%.2f", y) +
                "," + String.format("%.2f", z) +
                ')';
    }
}
