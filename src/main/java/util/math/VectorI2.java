/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util.math;

/**
 * This class implements an immutable, comparable vector in I^2 space.
 *
 * @author  Christian Sommer
 * @since   1/22/2015
 */
public class VectorI2 implements Comparable<VectorI2> {
    /**
     * Null Vector
     */
    public static final VectorI2 NULL = new VectorI2(0, 0);

    /**
     * Immutable vector values(coordinates).
     */
    public final int x, y;

    /**
     * Basic vector constructor. Since this class is immutable, all values need to be set.
     *
     * @param x x-value
     * @param y y-value
     */
    public VectorI2(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Adds two vectors and returns the sum.
     *
     * @param a summand 1
     * @param b summand 2
     * @return Sum of the two vectors.
     */
    public static VectorI2 add(VectorI2 a, VectorI2 b) {
        return new VectorI2(a.x + b.x, a.y + b.y);
    }

    /**
     * Subtracts two vectors and returns the difference.
     *
     * @param a minuend
     * @param b subtrahend
     * @return difference of the two vectors.
     */
    public static VectorI2 sub(VectorI2 a, VectorI2 b) {
        return new VectorI2(a.x - b.x, a.y - b.y);
    }

    /**
     * Multiplies two vectors and returns the product.
     *
     * @param a factor 1
     * @param b factor 2
     * @return product of the two vectors.
     */
    public static VectorI2 mul(VectorI2 a, VectorI2 b) {
        return new VectorI2(a.x * b.x, a.y * b.y);
    }

    /**
     * Multiplies one vector with a scalar value and returns the product.
     *
     * @param a factor 1
     * @param b factor 2
     * @return product of the vector and the scalar value
     */
    public static VectorI2 mul(VectorI2 a, int b) {
        return new VectorI2(a.x * b, a.y * b);
    }

    /**
     * Calculates the dot product of two vectors.
     *
     * @param a vector 1
     * @param b vector 2
     * @return dot product of the two vectors
     */
    public static double dot(VectorI2 a, VectorI2 b) {
        return a.x * b.x + a.y * b.y;
    }

    /**
     * Calculates the squared distance between two vectors.
     *
     * @param a vector 1
     * @param b vector 2
     * @return the squared distance between the two vectors
     */
    public static double distSqr(VectorI2 a, VectorI2 b) {
        final VectorI2 dif = sub(b, a);
        return dot(dif, dif);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        VectorI2 other = (VectorI2) obj;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        return true;
    }

    @Override
    public int compareTo(VectorI2 o) {
        return (x == o.x && y == o.y) ? 0 : 1;
    }

    @Override
    public String toString() {
        return "(" +
                "" + x +
                "," + y +
                ')';
    }
}
