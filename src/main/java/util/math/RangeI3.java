/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util.math;

/**
 * This immutable class implements a range in R^3 (a Box).
 *
 * @author  Christian Sommer
 * @since   6/2/2013 9:43 PM
 */
public class RangeI3 {
    /**
     * Minimal boundaries
     */
    public final VectorI3 min;

    /**
     * Maximal boundaries
     */
    public final VectorI3 max;

    /**
     * Basic constructor. Since this class is immutable, all values must be set.
     *
     * @param min  minimal boundaries
     * @param max  maximal boundaries
     */
    public RangeI3(VectorI3 min, VectorI3 max) {
        this.min = min;
        this.max = max;
    }

    /**
     * Calculates the volume of the box/range
     * @return volume value
     */
    public int getVolume() {
        return (max.x - min.x) * (max.y - min.y) * (max.z - min.z);
    }
}