/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util.math;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This implements a generic, serializable three-dimensional array.
 * <p>
 * This is the heart of our voxel engine. The higher order functions
 * implemented here were the main reason to create this project and
 * to use Java 8(+).
 *
 * @author  Christian Sommer
 * @since   6/1/2013
 */
public class Array3D<U> implements Serializable {
    /**
     * basic array
     */
    protected final ArrayList<U> array;

    /**
     * width of the array
     */
    protected final int width;

    /**
     * height of the array
     */

    protected final int height;

    /**
     * depth of the array
     */
    protected final int depth;

    /**
     * Constructor for serialization. <b>NEVER use this in handwritten code!</b>
     */
    protected Array3D() {
        super();
        this.width = -1;
        this.height = -1;
        this.depth = -1;
        this.array = null;
    }

    /**
     * Constructor which only sets the boundaries.
     *
     * @param width   width of the array
     * @param height  height of the array
     * @param depth   depth of the array
     */
    public Array3D(int width, int height, int depth) {
        super();
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.array = new ArrayList<>(width * height * depth);
    }

    /**
     * Constructor which sets the boundaries and invokes a initial fill function.
     *
     * @param width    width of the array
     * @param height   height of the array
     * @param depth    depth of the array
     * @param initFunc init function
     */
    public Array3D(int width, int height, int depth, InitFunction<U> initFunc) {
        super();
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.array = new ArrayList<>(width * height * depth);

        for (int i = 0; i < width * height * depth; i++)
            this.array.add(null);
        transform((x, y, z, i) -> initFunc.init(x, y, z));
    }

    /**
     * Constructor which sets the boundaries and fills the array with a constant value.
     *
     * @param width   width of the array
     * @param height  height of the array
     * @param depth   depth of the array
     * @param initval constant init value
     */
    public Array3D(int width, int height, int depth, U initval) {
        super();
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.array = new ArrayList<>(width * height * depth);

        for (int i = 0; i < width * height * depth; i++)
            this.array.add(initval);
    }

    /**
     * Returns the value for the given coordinates
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @return value for the given coordinates
     */
    public U get(int x, int y, int z) {
        return array.get(getFlatLocation(x, y, z));
    }

    /**
     * Sets the value for the given coordinates
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @param element new value for the given coordinates
     * @return the element previously at the specified position
     */
    public U set(int x, int y, int z, U element) {
        return array.set(getFlatLocation(x, y, z), element);
    }

    /**
     * Returns the width of the array
     *
     * @return width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the height of the array
     *
     * @return height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns the depth of the array
     *
     * @return depth
     */
    public int getDepth() {
        return depth;
    }

    /**
     * Transforms (changes the inner state) the complete array through the given function/lambda.
     *
     * @param transformFunction transformation function
     */
    public void transform(TransformFunction<U> transformFunction) {
        transform(0, 0, 0, width, height, depth, transformFunction);
    }

    /**
     * Transforms (changes the inner state) a sub range of the array through the given function/lambda.
     *
     * @param x0 minimal x
     * @param y0 minimal y
     * @param z0 minimal z
     * @param x1 maximal x
     * @param y1 maximal y
     * @param z1 maximal z
     * @param transformFunction transformation function
     */
    public void transform(int x0, int y0, int z0, int x1, int y1, int z1, TransformFunction<U> transformFunction) {
        for (int y = y0; y < y1; y++)
            for (int x = x0; x < x1; x++)
                for (int z = z0; z < z1; z++)
                    set(x, y, z, transformFunction.execute(x, y, z, get(x, y, z)));
    }

    /**
     * Iterates over the array through the given function/lambda.
     *
     * @param mapXYZFunction iteration function
     */
    public void iterate(IterationFunction<U> mapXYZFunction) {
        iterate(0, 0, 0, width, height, depth, mapXYZFunction);
    }

    /**
     * Iterates over a sub range of the array through the given function/lambda.
     *
     * @param x0 minimal x
     * @param y0 minimal y
     * @param z0 minimal z
     * @param x1 maximal x
     * @param y1 maximal y
     * @param z1 maximal z
     * @param iterationFunction iteration function
     */
    public void iterate(int x0, int y0, int z0, int x1, int y1, int z1, IterationFunction<U> iterationFunction) {
        x0 = Math.max(0, x0);
        y0 = Math.max(0, y0);
        z0 = Math.max(0, z0);

        x1 = Math.min(width - 1, x1);
        y1 = Math.min(height - 1, y1);
        z1 = Math.min(depth - 1, z1);

        for (int y = y0; y <= y1; y++)
            for (int x = x0; x <= x1; x++)
                for (int z = z0; z <= z1; z++)
                    iterationFunction.execute(x, y, z, get(x, y, z));
    }

    /**
     * Reduces the array list to a single value starting from the left (= Left Fold)
     * <p>
     * <pre>{@code
     *                           /-- initval /-- final value
     * foldl :: (b -> a -> b) -> b -> [a] -> b
     *           ^ aggregate function  ^-- this object
     * }</pre>
     *
     * @param foldFunction fold function
     * @param initval initial aggregate value
     * @param <V> Value type
     * @return returns the aggregated value
     */
    public <V> V foldLeft(FoldFunction<V, U> foldFunction, V initval) {
        return foldLeft(0, 0, 0, width, height, depth, foldFunction, initval);
    }

    /**
     * Reduces a sub range of the array list to a single value starting from the left (= Left Fold)
     * <p>
     * <pre>{@code
     *                                                         /-- initval /-- final value
     * foldl :: i -> i -> i -> i -> i -> i -> (b -> a -> b) -> b -> [a] -> b
     *                                         ^ aggregate function  ^-- this object
     * }</pre>
     *
     * @param x0 minimal x
     * @param y0 minimal y
     * @param z0 minimal z
     * @param x1 maximal x
     * @param y1 maximal y
     * @param z1 maximal z
     * @param foldFunction fold function
     * @param initval initial aggregate value
     * @param <V> Value type
     * @return returns the aggregated value
     */
    public <V> V foldLeft(int x0, int y0, int z0, int x1, int y1, int z1, FoldFunction<V, U> foldFunction, V initval) {
        x0 = Math.max(0, x0);
        y0 = Math.max(0, y0);
        z0 = Math.max(0, z0);

        x1 = Math.min(width - 1, x1);
        y1 = Math.min(height - 1, y1);
        z1 = Math.min(depth - 1, z1);

        V val = initval;

        for (int y = y0; y <= y1; y++)
            for (int x = x0; x <= x1; x++)
                for (int z = z0; z <= z1; z++) {
                    val = foldFunction.eval(val, get(x, y, z), x, y, z);
                }

        return val;
    }

    /**
     * Reduces a sub range of the array list to a single value starting from the left (= Left Fold) while
     * mapping to another coordinate system.
     * <p>
     * <pre>{@code
     *                                                         /-- initval  /--mapFunc   /-- final value
     * foldl :: i -> i -> i -> i -> i -> i -> (b -> a -> b) -> b -> [a] -> (I3 -> I3) -> b
     *                                         ^ aggregate function  ^-- this object
     * }</pre>
     *
     * @param x0 minimal x
     * @param y0 minimal y
     * @param z0 minimal z
     * @param x1 maximal x
     * @param y1 maximal y
     * @param z1 maximal z
     * @param foldProjectedFunction projected fold function
     * @param initval initial aggregate value
     * @param mappingFunction mapping function
     * @param <V> Value type
     * @return returns the aggregated value
     */
    public <V> V foldLeft(int x0, int y0, int z0, int x1, int y1, int z1,
                          FoldProjectedFunction<V, U> foldProjectedFunction,
                          V initval, MappingFunction mappingFunction) {
        V val = initval;

        for (int y = y0; y <= y1; y++)
            for (int x = x0; x <= x1; x++)
                for (int z = z0; z <= z1; z++) {
                    final VectorI3 mapped = mappingFunction.map(x, y, z);

                    if (mapped.x < 0 ||
                            mapped.y < 0 ||
                            mapped.z < 0 ||
                            mapped.x > getWidth() - 1 ||
                            mapped.y > getHeight() - 1 ||
                            mapped.z > getDepth() - 1)
                        continue;

                    val = foldProjectedFunction.eval(val, get(mapped.x, mapped.y, mapped.z),
                            mapped.x, mapped.y, mapped.z, x, y, z);
                }

        return val;
    }

    /**
     * Calculates the flat location of a R^3 coordinate
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @return flat location
     */
    private int getFlatLocation(int x, int y, int z) {
        return y * width * depth + x * depth + z;
    }

    /**
     * Interface for the constructor initial function.
     *
     * @param <V> Value type
     */
    public interface InitFunction<V> {
        /**
         * Returns a value for a given coordinate.
         *
         * @param x x-coordinate
         * @param y y-coordinate
         * @param z z-coordinate
         * @return value for the given coordinate
         */
        V init(int x, int y, int z);
    }

    /**
     * Interface for a mapping function between two coordinate systems
     * (especially from diamond mapping to real and vice versa).
     */
    public interface MappingFunction {
        /**
         * Maps a given position from one coordinate system to another.
         *
         * @param x x-coordinate
         * @param y y-coordinate
         * @param z z-coordinate
         * @return mapped coordinates
         */
        VectorI3 map(int x, int y, int z);
    }

    /**
     * Transforms a value for given coordinate into another value.
     *
     * @param <V> Value type
     */
    public interface TransformFunction<V> {
        /**
         * Transforms a value for given coordinate into another value.
         *
         * @param x x-coordinate
         * @param y y-coordinate
         * @param z z-coordinate
         * @param v current value
         * @return new value
         */
        V execute(int x, int y, int z, V v);
    }

    /**
     * Returns a value for a given coordinate, but does not change everything.
     *
     * @param <V> Value type
     */
    public interface IterationFunction<V> {
        /**
         * Returns a value for a given coordinate, but does not change everything.
         *
         * @param x x-coordinate
         * @param y y-coordinate
         * @param z z-coordinate
         * @param v current value
         */
        void execute(int x, int y, int z, V v);
    }

    /**
     * Reduction function for left fold.
     *
     * @param <U> Aggregation Type
     * @param <V> Value Type
     */
    public interface FoldFunction<U, V> {
        /**
         * Reduction function for left fold.
         *
         * @param value current aggregated value
         * @param element Value of the given coordinate
         * @param x x-coordinate
         * @param y y-coordinate
         * @param z z-coordinate
         * @return new aggregated value
         */
        U eval(U value, V element, int x, int y, int z);
    }

    /**
     * Reduction function for projected left fold.
     *
     * @param <U> Aggregation Type
     * @param <V> Value Type
     */
    public interface FoldProjectedFunction<U, V> {
        /**
         * Reduction function for projected left fold.
         *
         * @param value current aggregated value
         * @param element Value of the given coordinate
         * @param x x-coordinate
         * @param y y-coordinate
         * @param z z-coordinate
         * @param px projected x-coordinate
         * @param py projected y-coordinate
         * @param pz projected z-coordinate
         * @return new aggregated value
         */
        U eval(U value, V element, int x, int y, int z, int px, int py, int pz);
    }
}
