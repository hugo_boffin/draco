/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util.math;

/**
 * This class implements an immutable, comparable vector in I^3 space.
 *
 * @author  Christian Sommer
 * @since   6/2/2013 9:21 PM
 */
public class VectorI3 implements Comparable<VectorI3> {
    /**
     * Null Vector
     */
    public static final VectorI3 NULL = new VectorI3(0, 0, 0);

    /**
     * Immutable vector values(coordinates).
     */
    public final int x, y, z;

    /**
     * Basic vector constructor. Since this class is immutable, all values need to be set.
     *
     * @param x x-value
     * @param y y-value
     * @param z z-value
     */
    public VectorI3(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Adds two vectors and returns the sum.
     *
     * @param a summand 1
     * @param b summand 2
     * @return Sum of the two vectors.
     */
    public static VectorI3 add(VectorI3 a, VectorI3 b) {
        return new VectorI3(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    /**
     * Subtracts two vectors and returns the difference.
     *
     * @param a minuend
     * @param b subtrahend
     * @return difference of the two vectors.
     */
    public static VectorI3 sub(VectorI3 a, VectorI3 b) {
        return new VectorI3(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    /**
     * Multiplies two vectors and returns the product.
     *
     * @param a factor 1
     * @param b factor 2
     * @return product of the two vectors.
     */
    public static VectorI3 mul(VectorI3 a, VectorI3 b) {
        return new VectorI3(a.x * b.x, a.y * b.y, a.z * b.z);
    }

    /**
     * Multiplies one vector with a scalar value and returns the product.
     *
     * @param a factor 1
     * @param b factor 2
     * @return product of the vector and the scalar value
     */
    public static VectorI3 mul(VectorI3 a, int b) {
        return new VectorI3(a.x * b, a.y * b, a.z * b);
    }

    /**
     * Calculates the dot product of two vectors.
     *
     * @param a vector 1
     * @param b vector 2
     * @return dot product of the two vectors
     */
    public static double dot(VectorI3 a, VectorI3 b) {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    /**
     * Calculates the squared distance between two vectors.
     *
     * @param a vector 1
     * @param b vector 2
     * @return the squared distance between the two vectors
     */
    public static double distSqr(VectorI3 a, VectorI3 b) {
        final VectorI3 dif = sub(b, a);
        return dot(dif, dif);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        result = prime * result + z;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        VectorI3 other = (VectorI3) obj;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        if (z != other.z)
            return false;
        return true;
    }

    @Override
    public int compareTo(VectorI3 o) {
        return (x == o.x && y == o.y && z == o.z) ? 0 : 1;
    }

    @Override
    public String toString() {
        return "(" +
                "" + x +
                "," + y +
                "," + z +
                ')';
    }
}
