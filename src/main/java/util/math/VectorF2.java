/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util.math;

/**
 * This class implements an immutable, comparable vector in I^2 space.
 *
 * @author  Christian Sommer
 * @since   11/30/2013 1:39 PM
 */
public class VectorF2 implements Comparable<VectorF2> {
    /**
     * Null Vector
     */
    public static final VectorF2 NULL = new VectorF2(0, 0);

    /**
     * Immutable vector values(coordinates).
     */
    public final double x, y;

    /**
     * Basic vector constructor. Since this class is immutable, all values need to be set.
     *
     * @param x x-value
     * @param y y-value
     */
    public VectorF2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Adds two vectors and returns the sum.
     *
     * @param a summand 1
     * @param b summand 2
     * @return Sum of the two vectors.
     */
    public static VectorF2 add(VectorF2 a, VectorF2 b) {
        return new VectorF2(a.x + b.x, a.y + b.y);
    }

    /**
     * Subtracts two vectors and returns the difference.
     *
     * @param a minuend
     * @param b subtrahend
     * @return difference of the two vectors.
     */
    public static VectorF2 sub(VectorF2 a, VectorF2 b) {
        return new VectorF2(a.x - b.x, a.y - b.y);
    }

    /**
     * Multiplies two vectors and returns the product.
     *
     * @param a factor 1
     * @param b factor 2
     * @return product of the two vectors.
     */
    public static VectorF2 mul(VectorF2 a, VectorF2 b) {
        return new VectorF2(a.x * b.x, a.y * b.y);
    }

    /**
     * Multiplies one vector with a scalar value and returns the product.
     *
     * @param a factor 1
     * @param b factor 2
     * @return product of the vector and the scalar value
     */
    public static VectorF2 mul(VectorF2 a, double b) {
        return new VectorF2(a.x * b, a.y * b);
    }

    /**
     * Normalizes a vector (scale a vector to length = 1).
     *
     * @param v vector to normalize
     * @return normalized vector
     */
    public static VectorF2 normalize(VectorF2 v) {
        double mag = Math.sqrt(v.x * v.x + v.y * v.y);
        if (mag == 0)
            return NULL;
        return new VectorF2(v.x / mag, v.y / mag);
    }

    /**
     * Calculates the dot product of two vectors.
     *
     * @param a vector 1
     * @param b vector 2
     * @return dot product of the two vectors
     */
    public static double dot(VectorF2 a, VectorF2 b) {
        return a.x * b.x + a.y * b.y;
    }

    /**
     * Calculates the squared distance between two vectors.
     *
     * @param a vector 1
     * @param b vector 2
     * @return the squared distance between the two vectors
     */
    public static double distSqr(VectorF2 a, VectorF2 b) {
        final VectorF2 dif = sub(b, a);
        return dot(dif, dif);
    }

    /**
     * Calculates the perpendicular vector.
     *
     * @return perpendicular vector
     */
    public VectorF2 perpendicular() {
        return new VectorF2(-y, x);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VectorF2 vectorF2 = (VectorF2) o;

        if (Double.compare(vectorF2.x, x) != 0) return false;
        if (Double.compare(vectorF2.y, y) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public int compareTo(VectorF2 o) {
        return (x == o.x && y == o.y) ? 0 : 1;
    }

    @Override
    public String toString() {
        return "(" +
                "" + String.format("%.2f", x) +
                "," + String.format("%.2f", y) +
                ')';
    }
}
