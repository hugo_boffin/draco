/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util.tiles;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.IntStream;

/**
 * This class generate artifical voxel images.
 *
 * @author  Christian Sommer
 * @since   7/1/2013
 */
public class TileGenerator {
    private static final Logger logger = LogManager.getLogger(TileGenerator.class.getName());

    private final int tileSize;
    private final boolean drawChecker;
    private final boolean drawUnitCube;
    private final Color lineColor;
    private final boolean drawOutlines;
    private final boolean drawPickingBox;

    /**
     * Creates a Tile Generator without an outline, checker and unit cube. Line color is white. Tile size is 32.
     */
    public TileGenerator() {
        lineColor = Color.WHITE;
        drawUnitCube = false;
        drawChecker = false;
        tileSize = 64;
        drawOutlines = false;
        this.drawPickingBox = true;
    }

    /**
     * Creates a customized Tile Generator.
     *
     * @param tileSize          Size of a voxel tile
     * @param drawChecker       If true, draws a checker board behind the image
     * @param drawUnitCube      If true, draws a unit cube above the image
     * @param drawOutlines      If true, draws line outlines
     * @param lineColor         Outline color
     * @param drawPickingBox    Draws the Mouse Picking Boxes into the map
     */
    public TileGenerator(int tileSize, boolean drawChecker, boolean drawUnitCube, boolean drawOutlines, Color lineColor, boolean drawPickingBox) {
        this.tileSize = tileSize;
        this.drawChecker = drawChecker;
        this.drawUnitCube = drawUnitCube;
        this.drawOutlines = drawOutlines;
        this.lineColor = lineColor;
        this.drawPickingBox = drawPickingBox;
    }

    /**
     * Renders a voxel tile into an image.
     *
     * @param ground GroundType definition for the voxel
     * @return BufferedImage containing the rendered tile
     */
    public BufferedImage renderTile(GroundType ground) {
        BufferedImage bImg = new BufferedImage(4 * tileSize, 4 * tileSize, BufferedImage.TYPE_INT_ARGB);
        Graphics2D cg = bImg.createGraphics();
        paint(cg, ground);
        return bImg;
    }

    /**
     * Static method that writes a buffered image to a png file
     *
     * @param name Filename of the image
     * @param bImg buffered image to save
     */
    public static void saveToFile(String name, BufferedImage bImg) {
        try {
            if (ImageIO.write(bImg, "png", new File("" + name + ".png"))) {
                logger.info("Saved Tile Map to " + name);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Draws a single tile into an graphics object.
     *
     * @param g Graphics2D context
     * @param ox position x-coordinate
     * @param oy position x-coordinate
     * @param index tile index
     * @param ground GroundType definiton
     */
    public void drawSingleTile(Graphics2D g, int ox, int oy, int index, GroundType ground) {
        g.setColor(lineColor);

        if (drawUnitCube)
            drawUnitCube(g, ox, oy);

        if (drawPickingBox) {
            g.setColor(Color.RED);
            g.fillRect(ox,oy,tileSize/2,tileSize-tileSize/4);
            g.setColor(Color.YELLOW);
            g.fillRect(ox+tileSize/2,oy,tileSize/2,tileSize-tileSize/4);
            g.setColor(Color.GREEN);
            g.fillRect(ox,oy+tileSize-tileSize/4,tileSize/2,tileSize/4);
            g.setColor(Color.BLUE);
            g.fillRect(ox+tileSize/2,oy+tileSize-tileSize/4,tileSize/2,tileSize/4);
        }

        BigInteger bs = new BigInteger("" + index);

        int x0 = bs.testBit(0) ? 0 : 1;
        int x1 = bs.testBit(1) ? 0 : 1;
        int x2 = bs.testBit(2) ? 0 : 1;
        int x3 = bs.testBit(3) ? 0 : 1;
        //g.drawString(""+(1-x0)+(1-x1)+(1-x2)+(1-x3), ox,oy+_128);

        final int level = tileSize / 2;

        Point p00 = new Point(ox, x0 * level + oy + tileSize / 4);
        Point p01 = new Point(ox + tileSize / 2 - 1, x1 * level + oy + tileSize / 4 + tileSize / 4 - 1);
        Point p10 = new Point(ox + tileSize / 2, x1 * level + oy + tileSize / 4 + tileSize / 4 - 1);
        Point p11 = new Point(ox + tileSize - 1, x2 * level + oy + tileSize / 4);
        Point p20 = new Point(ox + tileSize - 1, x2 * level + oy + tileSize / 4 - 1);
        Point p21 = new Point(ox + tileSize / 2, x3 * level + oy + tileSize / 4 - tileSize / 4);
        Point p30 = new Point(ox + tileSize / 2 - 1, x3 * level + oy + tileSize / 4 - tileSize / 4);
        Point p31 = new Point(ox, x0 * level + oy + tileSize / 4 - 1);

        Point p00_0 = new Point(ox, level + oy + tileSize / 4);
        Point p01_0 = new Point(ox + tileSize / 2 - 1, level + oy + tileSize / 4 + tileSize / 4 - 1);
        Point p10_0 = new Point(ox + tileSize / 2, level + oy + tileSize / 4 + tileSize / 4 - 1);
        Point p11_0 = new Point(ox + tileSize - 1, level + oy + tileSize / 4);

        // Grundfläche
        drawParLines(g, ground.TopColor, p00.x, p01.x, p31.y, p30.y, p00.y, p01.y, false);
        drawParLines(g, ground.TopColor, p10.x, p11.x, p21.y, p20.y, p10.y, p11.y, false);

        // Linke Seite
        if (x0 != 1 || x1 != 1)
            drawParLines(g, ground.Side1Color, p00_0.x, p01_0.x, p00.y, p01.y, p00_0.y, p01_0.y, true);

        // Rechte Seite
        if (x1 != 1 || x2 != 1)
            drawParLines(g, ground.Side2Color, p10_0.x, p11_0.x, p10.y, p11.y, p10_0.y, p11_0.y, true);

        // Tile 7 - ?
        /*
        if (index == 7) {
            p = new Polygon();
            p.addPoint(ox + p0.x, oy + p0.y);
            p.addPoint(ox + p1.x, oy + p1.y);
            p.addPoint(ox + p2.x, oy + p2.y);
            drawElement(g, ground, p, ground.TopColor);
        }*/

        // -   0000010010110100
        /* if (Arrays.asList(5,8,10,11,13).contains(index)) {
            g.drawLine(ox + p1.x, oy + p1.y, ox + p3.x, oy + p3.y);
        } */
    }

    private void paint(Graphics2D g, GroundType ground) {
//		    g.setBackground(TRANSPARENT_COLOR);

        g.setFont(new Font("Dialog", Font.PLAIN, 10));

        int ox = 0;
        int oy = 0;

        if (drawChecker)
            drawCheckerBoard(g);

        for (int i = 0; i < 16; i++) {
            drawSingleTile(g, ox, oy, i, ground);

            if ((i + 1) % 4 == 0) {
                ox = 0;
                oy += tileSize;
            } else
                ox += tileSize;
        }
    }

    private void drawPixel(Graphics2D g, Point p) {
        g.drawRect(p.x, p.y, 0, 0);
    }

    private void drawParLines(Graphics2D g, Color color, int x0, int x1, int y0, int y1, int y2, int y3, boolean lrLines) {
        Color lc = new Color(
                (float) lineColor.getRed() / 255.0f * 0.5f + 0.5f * (float) color.getRed() / 255.0f
                , (float) lineColor.getGreen() / 255.0f * 0.5f + 0.5f * (float) color.getGreen() / 255.0f
                , (float) lineColor.getBlue() / 255.0f * 0.5f + 0.5f * (float) color.getBlue() / 255.0f
        );

        ArrayList<Point> alo = lineLst(x0, y0, x1, y1);
        ArrayList<Point> blo = lineLst(x0, y2, x1, y3);
        IntStream.range(x0, x1 + 1).forEach(x -> {
            final int fx = x;
            Optional<Point> min = alo.stream().filter(v -> v.x == fx).min(Comparator.comparingInt(o -> o.y));
            Optional<Point> max = blo.stream().filter(v -> v.x == fx).max(Comparator.comparingInt(o -> o.y));

            IntStream.range(min.get().y, max.get().y + 1).forEach(y -> {
                Point p = new Point(x, y);
                if (drawOutlines &&
                        (lrLines && (x == x0 || x == x1) ||
                                alo.contains(p) || blo.contains(p))
                        )
                    g.setColor(lc);
                else
                    g.setColor(color);
                drawPixel(g, p);
            });
        });
    }

    private void line(int x0, int y0, int x1, int y1, Graphics2D g) {
        Bresenham.Line(x0, y0, x1, y1, (x, y) -> drawPixel(g, new Point(x, y)));
    }

    private ArrayList<Point> lineLst(int x0, int y0, int x1, int y1) {
        ArrayList<Point> pts = new ArrayList<>();
        Bresenham.Line(x0, y0, x1, y1, (x, y) -> pts.add(new Point(x, y)));
        return pts;
    }

    private void drawCheckerBoard(Graphics2D g) {
        g.setColor(Color.green);
        g.fillRect(0, 0, tileSize, tileSize);
        g.fillRect(2 * tileSize, 0, tileSize, tileSize);
        g.fillRect(tileSize, tileSize, tileSize, tileSize);
        g.fillRect(3 * tileSize, tileSize, tileSize, tileSize);
        g.fillRect(0, 2 * tileSize, tileSize, tileSize);
        g.fillRect(2 * tileSize, 2 * tileSize, tileSize, tileSize);
        g.fillRect(tileSize, 3 * tileSize, tileSize, tileSize);
        g.fillRect(3 * tileSize, 3 * tileSize, tileSize, tileSize);
        g.setColor(Color.red);
        g.fillRect(tileSize, 0, tileSize, tileSize);
        g.fillRect(3 * tileSize, 0, tileSize, tileSize);
        g.fillRect(0, tileSize, tileSize, tileSize);
        g.fillRect(2 * tileSize, tileSize, tileSize, tileSize);
        g.fillRect(tileSize, 2 * tileSize, tileSize, tileSize);
        g.fillRect(3 * tileSize, 2 * tileSize, tileSize, tileSize);
        g.fillRect(0, 3 * tileSize, tileSize, tileSize);
        g.fillRect(2 * tileSize, 3 * tileSize, tileSize, tileSize);
    }

    private void drawUnitCube(Graphics2D g, int ox, int oy) {
        g.drawLine(ox, oy, ox + tileSize / 2, oy + tileSize / 4);
        g.drawLine(ox + tileSize, oy, ox + tileSize / 2, oy + tileSize / 4);
        g.drawLine(ox, oy, ox + tileSize / 2, oy - tileSize / 4);
        g.drawLine(ox + tileSize, oy, ox + tileSize / 2, oy - tileSize / 4);

        g.drawLine(ox, oy + tileSize / 2, ox + tileSize / 2, oy + tileSize / 4 + tileSize / 2);
        g.drawLine(ox + tileSize, oy + tileSize / 2, ox + tileSize / 2, oy + tileSize / 4 + tileSize / 2);
        g.drawLine(ox, oy + tileSize / 2, ox + tileSize / 2, oy - tileSize / 4 + tileSize / 2);
        g.drawLine(ox + tileSize, oy + tileSize / 2, ox + tileSize / 2, oy - tileSize / 4 + tileSize / 2);

        g.drawLine(ox, oy + tileSize / 2, ox, oy);
        g.drawLine(ox + tileSize, oy + tileSize / 2, ox + tileSize, oy);
        g.drawLine(ox + tileSize / 2, oy + 92, ox + tileSize / 2, oy - tileSize / 4);
    }

    private interface PointCallback {
        void run(int x, int y);
    }

    private static class Bresenham {
        public static void Line(int x0, int y0, int x1, int y1, PointCallback call) {
            int dx = Math.abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
            int dy = -Math.abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
            int err = dx + dy, e2; /* error value e_xy */

            for (; ; ) {  /* loop */
                call.run(x0, y0);
                if (x0 == x1 && y0 == y1) break;
                e2 = 2 * err;
                if (e2 > dy) {
                    err += dy;
                    x0 += sx;
                } /* e_xy+e_x > 0 */
                if (e2 < dx) {
                    err += dx;
                    y0 += sy;
                } /* e_xy+e_y < 0 */
            }
        }
    }
}
