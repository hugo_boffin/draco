/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util.tiles;

import java.awt.*;

/**
 * Visual definition for an auto-generated voxel image/map.
 *
 * @author  Christian Sommer
 * @since   6/3/2013 8:22 PM
 */
public class GroundType {
    public final String Name;
    public final Color TopColor;
    public final Color Side1Color;
    public final Color Side2Color;

    /**
     * Constructor for a GroundType with the same side colors.
     *
     * @param name Name of this ground type(water, sand, grass, ...)
     * @param topColor Top color of the block
     * @param sideColor Side colors of the block
     */
    public GroundType(String name, Color topColor, Color sideColor) {
        this.Name = name;
        TopColor = topColor;
        Side1Color = sideColor;
        Side2Color = sideColor;
    }

    /**
     * Constructor for a GroundType with different side colors.
     *
     * @param name Name of this ground type(water, sand, grass, ...)
     * @param topColor Top color of the block
     * @param side1Color Side color 1 of the block
     * @param side2Color Side color 2 of the block
     */
    public GroundType(String name, Color topColor, Color side1Color, Color side2Color) {
        this.Name = name;
        TopColor = topColor;
        Side1Color = side1Color;
        Side2Color = side2Color;
    }
}
