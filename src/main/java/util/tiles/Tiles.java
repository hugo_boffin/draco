/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util.tiles;

import java.awt.Color;
import java.util.List;

/**
 * This is a simple default tile map. It provides definitions for grass,sand, water and a gray block.
 *
 * @author Christian Sommer
 * @since 6/12/2013 9:42 PM
 */
public class Tiles {
    public static final List<GroundType> DEFAULT = List.of(
            new GroundType("grass", new Color(0x6B, 0xCE, 0x23), new Color(0x96, 0x4B, 0x00), new Color(0x7d, 0x3e, 0x00)),
            new GroundType("sand", new Color(0xDE, 0xC6, 0x83), new Color(0xDE, 0xC6, 0x83).darker(), new Color(0xDE, 0xC6, 0x83).darker().darker()),
            new GroundType("water", new Color(65, 146, 255), new Color(65, 146, 255).darker(), new Color(65, 146, 255).darker().darker()),
            new GroundType("gray", new Color(160, 160, 160), Color.LIGHT_GRAY)
    );
    public static final List<GroundType> LIGHT_TEST = List.of(
            new GroundType("grass", Color.WHITE, Color.LIGHT_GRAY, Color.LIGHT_GRAY),
            new GroundType("sand", Color.WHITE, Color.LIGHT_GRAY, Color.LIGHT_GRAY),
            new GroundType("water", Color.WHITE, Color.LIGHT_GRAY, Color.LIGHT_GRAY),
            new GroundType("gray", Color.WHITE, Color.LIGHT_GRAY, Color.LIGHT_GRAY)
    );

}
