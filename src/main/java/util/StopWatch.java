/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util;

import org.apache.logging.log4j.Logger;

/**
 * This is a simple time measurement class for benchmarking.
 *
 * @author  Christian Sommer
 * @since   10/15/2014
 */
public class StopWatch {
    public interface MeasureFunction {
        void run();
    }

    private double start, stop;

    /**
     * Starts a measurement.
     */
    public void start() {
        start = System.nanoTime() / 1000000.0;
    }

    /**
     * Stops the measurement.
     *
     * @return time elapsed in ms
     */
    public double stop() {
        stop = System.nanoTime() / 1000000.0;

        return time();
    }

    /**
     * Returns the last time elapsed.
     *
     * @return time elapsed in ms
     */
    public double time() {
        return (stop - start);
    }

    /**
     * A convenient way to measure time. Just put in your relevant code.
     *
     * @param eventStr beginning log message. typically a function name
     * @param logger log4j logger
     * @param func function containing your code
     * @return time elapsed in ms
     */
    public static double measureAndLog(String eventStr, Logger logger,MeasureFunction func) {
        StopWatch watch = new StopWatch();

        watch.start();
        func.run();
        watch.stop();

        logger.info(eventStr + " took " + String.format("%.2f", watch.time()) + "ms");

        return watch.time();
    }
}
