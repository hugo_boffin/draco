/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main;

import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.MouseAdapter;
import com.jogamp.newt.event.MouseEvent;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.util.awt.TextRenderer;
import engine.Game;
import engine.GameDelegate;
import engine.PerformanceCounter;
import engine.input.Input;
import engine.input.StateKeyAction;
import engine.input.TriggerKeyAction;
import engine.renderer.RendererOptions;
import engine.renderer.blitter.Blitter;
import engine.renderer.blitter.BlitterFactory;
import engine.renderer.views.DebugRenderer;
import engine.renderer.views.WorldRenderer;
import model.Voxel;
import model.World;
import model.WorldFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picking.MousePicking;
import util.math.VectorF3;
import util.math.VectorI3;

import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Simple Game Demo. You can only scroll through the (endless) landscape at the moment.
 *
 * @author  Christian Sommer
 * @since   3/4/2015
 */
public class DemoGameDelegate implements GameDelegate {
    private final Game game;
    private final Logger logger;
    private final PerformanceCounter perfCounter;
    private final StateKeyAction moveDownKeyEvent = new StateKeyAction();
    private final StateKeyAction moveUpKeyEvent = new StateKeyAction();
    private final StateKeyAction moveLeftKeyEvent = new StateKeyAction();
    private final StateKeyAction moveRightKeyEvent = new StateKeyAction();
    private TextRenderer textRenderer;
    private Blitter blitter;
    private VectorF3 cameraPosition;
    private World world;
    private WorldRenderer worldRenderer;
    private final TriggerKeyAction toggleTilePositionKeyEvent = new TriggerKeyAction((e) ->
            worldRenderer.setDisplayDebugInfo(!worldRenderer.isDisplayDebugInfo()));
    private final TriggerKeyAction toggleDisplayUseRealCoordsKeyEvent = new TriggerKeyAction((e) ->
            worldRenderer.toggleCoordinatesDisplay());
    private MousePicking mousePicker;

    public DemoGameDelegate(Game game) {
        this.game = game;
        this.logger = LogManager.getLogger(GameDelegate.class.getName());
        this.perfCounter = new PerformanceCounter();
        this.cameraPosition = VectorF3.NULL;
    }

    @Override
    public String getTitle() {
        return "Draco Voxel Engine Prototype";
    }

    @Override
    public void initialize() {
        world = WorldFactory.create();

        final Input input = game.getInput();
        input.addKeyAction(KeyEvent.VK_LEFT, moveLeftKeyEvent);
        input.addKeyAction(KeyEvent.VK_RIGHT, moveRightKeyEvent);
        input.addKeyAction(KeyEvent.VK_UP, moveUpKeyEvent);
        input.addKeyAction(KeyEvent.VK_DOWN, moveDownKeyEvent);
        input.addKeyAction(KeyEvent.VK_A, moveLeftKeyEvent);
        input.addKeyAction(KeyEvent.VK_D, moveRightKeyEvent);
        input.addKeyAction(KeyEvent.VK_W, moveUpKeyEvent);
        input.addKeyAction(KeyEvent.VK_S, moveDownKeyEvent);
        input.addKeyAction(KeyEvent.VK_F1, toggleTilePositionKeyEvent);
        input.addKeyAction(KeyEvent.VK_F2, toggleDisplayUseRealCoordsKeyEvent);

        game.getGlWindow().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                VectorI3 mouseVoxel = mousePicker.getVoxelUnderMouse();
                if (mouseVoxel == null)
                    return;
                if (mouseEvent.getButton() == MouseEvent.BUTTON1) {
                    world.getTerrain().setVoxel(mouseVoxel.x,mouseVoxel.y,mouseVoxel.z,null);
                    logger.info("Deleting voxel {}", mouseVoxel);
                    mouseEvent.setConsumed(true);
                } else if (mouseEvent.getButton() == MouseEvent.BUTTON2) {
                    world.getTerrain().setVoxel(mouseVoxel.x,mouseVoxel.y,mouseVoxel.z,new Voxel(1,15));
                    logger.info("Adding voxel {}", mouseVoxel);
                    mouseEvent.setConsumed(true);
                }
            }
        });
    }

    @Override
    public void loadContent(GLAutoDrawable drawable) {
        try {
            GL2 gl = drawable.getGL().getGL2();

            blitter = BlitterFactory.create(RendererOptions.get().blitterType, gl);

            textRenderer = new TextRenderer(new Font(Font.SANS_SERIF, Font.PLAIN, 12));

            worldRenderer = new WorldRenderer(gl, world);

            mousePicker = new MousePicking(game, gl, world, blitter);
        } catch (GLException e) {
            logger.error("Unable to create OpenGL Context. Drivers up to date? JOGL Version up to date?");
        }
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL2 gl = drawable.getGL().getGL2();

        gl.glClearColor(0.5f, 0.5f, 0.9f, 0f);
        gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);

        gl.setSwapInterval(RendererOptions.get().waitForVerticalSync ? 1 : 0);

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(0, width, height, 0, -9999, 9999);

        gl.glMatrixMode(GL2.GL_MODELVIEW);
    }

    @Override
    public void update() {
        perfCounter.tick();

        VectorF3 offset = VectorF3.normalize(new VectorF3(
                (moveRightKeyEvent.isPressed() ? +1.0f : 0) + (moveLeftKeyEvent.isPressed() ? -1.0f : 0),
                (moveDownKeyEvent.isPressed() ? +1.0f : 0) + (moveUpKeyEvent.isPressed() ? -1.0f : 0), 0));

        cameraPosition = VectorF3.add(cameraPosition, VectorF3.mul(offset, perfCounter.getScale() * 1000.0));

        //TODO: Terrain Update Mouse Position - Delete/Insert/ ...
    }

    @Override
    public void draw(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glClear(GL.GL_COLOR_BUFFER_BIT);

        blitter.resetBlitCounter();

        worldRenderer.render(drawable, gl, blitter, cameraPosition);

        DebugRenderer.get().render(drawable, gl, blitter, cameraPosition);

//        mousePicker.findVoxelUnderMouse(gl,drawable, textRenderer, cameraPosition);

        drawStatistics(drawable);
    }

    private void drawStatistics(GLAutoDrawable drawable) {
        textRenderer.beginRendering(drawable.getSurfaceWidth(), drawable.getSurfaceHeight());
        textRenderer.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        renderStatusInfos(drawable);
        textRenderer.endRendering();
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
        world.getTerrain().shutDownExecutor();
    }

    private void renderStatusInfos(GLAutoDrawable drawable) {
        final String statistics = getStatistics();
        Rectangle2D bounds = textRenderer.getBounds(statistics);

        textRenderer.draw(statistics,
                drawable.getSurfaceWidth() - (int) bounds.getWidth(),
                drawable.getSurfaceHeight() - (int) bounds.getHeight());
    }

    private String getStatistics() {
        return String.format("%.2f fps, %d blits, %d cells visible, %d / %d cached cells, pos%s m(%d,%d), %.2fmb max, %.2fmb free, %.2fmb total",
                game.getAnimator().getLastFPS(), blitter.getBlitCount(),
                worldRenderer.getVisibleCellCount(),
                world.getTerrain().getCachedDoneCellCount(),
                world.getTerrain().getCachedCellCount(),
                cameraPosition.toString(),
                game.getInput().getMouseX(), game.getInput().getMouseY(),
                Runtime.getRuntime().maxMemory() / 1024.0 / 1024.0,
                Runtime.getRuntime().freeMemory() / 1024.0 / 1024.0,
                Runtime.getRuntime().totalMemory() / 1024.0 / 1024.0);
    }
}
