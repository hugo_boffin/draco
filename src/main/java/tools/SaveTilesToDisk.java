/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tools;

import util.tiles.GroundType;
import util.tiles.TileGenerator;
import util.tiles.Tiles;

import java.awt.*;

/**
 * This is a simple tool to save a tile map to disk.
 *
 * @author  Christian Sommer
 * @since   7/2/2013 8:09 PM
 */
public class SaveTilesToDisk {
    /**
     * entry point
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        final TileGenerator tg = new TileGenerator(64,false,false,true, Color.BLACK,false);
        Tiles.DEFAULT.forEach((g) -> tg.saveToFile(g.Name, tg.renderTile(g)));

        final TileGenerator  tgo = new TileGenerator(64,false,false,false, Color.WHITE,true);
        tg.saveToFile("picking",tgo.renderTile(new GroundType("picking",Color.WHITE,Color.WHITE,Color.WHITE)));
    }
}
