/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tools;

import util.noise.ImprovedNoise;

import javax.swing.*;
import java.awt.*;

/**
 * Generates an Improved Perlin Noise Map and displays it in a Swing window.
 *
 * @author  Christian Sommer
 * @since   7/1/2013
 */
public class NoiseGenerator {
    /**
     * entry point
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        Dimension size = new Dimension(640, 480);

        JFrame frame = new MyFrame();
        frame.setSize(size);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    @SuppressWarnings("serial")
    private static class MyFrame extends JFrame {
        @Override
        public void paint(Graphics g1) {
            super.paint(g1);
            Graphics2D g = (Graphics2D) g1;
            g.setBackground(Color.WHITE);

            // g.setColor(Color.green);
            // g.fillRect(0, 0, _128, _128);
            // g.fillRect(2*_128, 0, _128, _128);
            // g.fillRect(_128, _128, _128, _128);
            // g.fillRect(3*_128, _128, _128, _128);
            double z = 0.5;
            int f = 400;
            double scale = 2;
            double xw = 1.0 / (double) f;
            double xh = 1.0 / (double) f;

            double min = 5;
            double max = -5;

            for (float y = 0f; y <= 1f; y += xh) {
                for (float x = 0f; x <= 1f; x += xw) {
                    // for (float z = 0f; z <= 1f; z += xw) {
                    double v = ImprovedNoise.noise(x * scale, y * scale, z * scale);
                    final float farbe = (float) ((v + 1.0) * 0.5);
//					System.out.println(""+farbe);
                    g.setColor(new Color(farbe, farbe, farbe));
                    g.fillRect((int) (f * x), (int) (f * y), 1, 1);
                    min = Math.min(min, v);
                    max = Math.max(max, v);
                }
            }
            // }

            System.out.println(min + " " + max);
        }
    }
}
