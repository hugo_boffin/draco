/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tools;

import java.math.BigInteger;

/**
 * This is a simple tool to test my tile generator-evaluators
 *
 * @author  Christian Sommer
 * @since   6/12/2013 10:35 PM
 */
public class TileIndexEvaluator {
    /**
     * entry point
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        for (int i = 0; i < 16; i++) {
            BigInteger bs = new BigInteger("" + i);

            int x0 = bs.testBit(0) ? 0 : 1;
            int x1 = bs.testBit(1) ? 0 : 1;
            int x2 = bs.testBit(2) ? 0 : 1;
            int x3 = bs.testBit(3) ? 0 : 1;

            System.out.print(evaluatorFunc(i, x0, x1, x2, x3) ? "1" : "0");
        }
        System.out.println();
    }

    private static boolean evaluatorFunc(int i, int x0, int x1, int x2, int x3) {
        return x0 == x2 && (x0 + x1 + x2 + x3) % 4 != 0 &&
                i != 8 &&
                i != 13 &&
                i == 1;
    }
}
