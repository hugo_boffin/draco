/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tools;

import util.tiles.TileGenerator;
import util.tiles.Tiles;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Small tool writing a generated voxel image to a file.
 *
 * @author  Christian Sommer
 * @since   7/2/2013 8:12 PM
 */
public class SaveSingleTileToDisk {
    /**
     * entry point
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        TileGenerator tg = new TileGenerator();
        BufferedImage bImg = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
        Graphics2D cg = bImg.createGraphics();
        tg.drawSingleTile(cg, 0, 0, 1, Tiles.DEFAULT.get(0));
        tg.saveToFile("test", bImg);
    }
}
