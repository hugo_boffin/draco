/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model.generator;

/**
 * Interface for the base processing path of a {@link model.generator.ProceduralTerrainCreator}
 *
 * @author  Christian Sommer
 * @since   2/25/2015
 */
public interface BasePassFunction {
    /**
     * This method is invoked once for every cell. {@link model.generator.WorldToolkit}
     * comes preconfigured with cell, mask, etc. .
     *
     * @param toolkit concrete world toolkit
     */
    void compute(DefaultWorldToolkit toolkit);
}
