/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model.generator;

import model.Mapping;
import model.VoxelPatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import util.math.Array3D;
import util.math.GaussianCollector3D;
import util.math.Rectangle;

/**
 * Lights up an {@link model.VoxelPatch}.
 *
 * @author  Christian Sommer
 * @since   3/3/2015
 */
public class LightGenerator {
    private static final Logger logger = LogManager.getLogger(LightGenerator.class.getName());
    private final LightPassType lightPassType;
    private final int occlusionRadius;
    private final GaussianCollector3D gaussianCollector3D;
    private final int cellCount;

    /**
     * Creates a light generator with the given lighting model.
     *
     * @param lightPassType lighting model
     */
    public LightGenerator(LightPassType lightPassType) {
        this.lightPassType = lightPassType;
        this.occlusionRadius = 6;
        this.gaussianCollector3D = new GaussianCollector3D(occlusionRadius,false);
        this.cellCount = gaussianCollector3D.getCellCount();
    }

    /**
     * Illuminates a given cell
     *
     * @param cell unlit cell
     * @param overlapMapping mapping function
     */
    public void illuminateCell(VoxelPatch cell, Mapping overlapMapping) {
        switch (lightPassType) {
            case SIMPLE -> lightPassSimple(cell);
            case OCCLUSION -> lightPassOcclusion(cell);
        }
    }

    /**
     * Returns type of lighting.
     *
     * @return lighting type
     */
    public LightPassType getLightPassType() {
        return lightPassType;
    }

    /**
     * Returns occlusion radius.
     *
     * @return occlusion radius
     */
    public int getOcclusionRadius() {
        return occlusionRadius;
    }

    /**
     * Simple Light Pass just sets the voxels z as light color (the higher, the more light)
     *
     * @param patch voxel patch
     */
    private void lightPassSimple(VoxelPatch patch) {
        int alpha = 0xFF;
        patch.iterate((x, y, z, v) -> {
            if (v == null)
                return;

            double f = 0.4 + 0.6 * (((double) z) / ((double) patch.getDepth()));
            int b = (int) (Math.min(Math.max(f, 0), 1.0) * 255.0);

            int c = alpha;
            c = (c << 8) | b;
            c = (c << 8) | b;
            c = (c << 8) | b;

            patch.lightPatch.set(x, y, z, c);
        });
    }

    /**
     * Occlusion Light Pass calculates a 3-pass gaussian and collects all filled cells around a voxel
     * (depending on radius). The more voxel are around, the less light comes through.
     *
     * @param patch voxel patch
     */
    private void lightPassOcclusion(VoxelPatch patch) {
        Array3D<Integer> gaussianLightMap = gaussianCollector3D.compute(patch,0,0,(o,v) -> v!=null?++o:o, Integer::sum);

        patch.lightPatch.transform((x, y, z, v) -> {
            if (v == null)
                return null;

            final int fillCount = gaussianLightMap.get(x, y, z);
            if (fillCount > cellCount)
                logger.warn("FillCount>CellCount - Impossible...");

            short l = (short) ((short) 0x40 + (short) ((double) 0x40 * (double) z / (double) patch.getDepth()));
            final double litLevel = 1.0 - (double) fillCount / (double) cellCount;
            l += (short) ((double) 0x7F * litLevel);

            int c = 0xFF;
            c = (c << 8) | l;
            c = (c << 8) | l;
            c = (c << 8) | l;

            return c;
        });
    }

    /**
     * Returns the required cell overlapping for correct lighting.
     *
     * @return required cell overlapping
     */
    public Rectangle getOverlapping() {
        return gaussianCollector3D.getOverlapping();
    }

    /**
     * Type of Lighting Model used.
     */
    public enum LightPassType {
        /**
         * Simple height based lighting
         */
        SIMPLE,
        /**
         * Occlusion based lighting
         */
        OCCLUSION
    }
}
