/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model.generator;

import org.nustaq.serialization.FSTConfiguration;
import model.Mapping;
import model.VoxelPatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import util.math.VectorI3;

import java.io.*;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

/**
 * A Decorator for another {@link model.generator.TerrainCreator}. Tries to load cells from disk, otherwise creates them.
 *
 * @author  Christian Sommer
 * @since   1/17/2015
 */
public class CachedTerrainCreator implements TerrainCreator {
    private static final Logger logger = LogManager.getLogger(CachedTerrainCreator.class.getName());
    private static final transient FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
    private final boolean compressCells;
    private final TerrainCreator boxedCreator;

    /**
     * Creates a given CachedTerrainCreator with a boxed Creator. Cell Compression is activated.
     *
     * @param boxedCreator boxed TerrainCreator
     */
    public CachedTerrainCreator(TerrainCreator boxedCreator) {
        this.boxedCreator = boxedCreator;
        this.compressCells = true;
    }

    /**
     * Creates a given CachedTerrainCreator with a boxed Creator. Cell Compression can be turned off.
     *
     * @param compressCells Compress saved Cells
     * @param boxedCreator boxed TerrainCreator
     */
    public CachedTerrainCreator(boolean compressCells, TerrainCreator boxedCreator) {
        this.compressCells = compressCells;
        this.boxedCreator = boxedCreator;
    }

    @Override
    public VoxelPatch createCell(Mapping mapping, VectorI3 cellIndex) {
        VoxelPatch patch = null;

        final VectorI3 cellSize = boxedCreator.getCellSize();

        File f = new File("cache/" + "Cell_" + cellIndex.x / cellSize.x + "_" + cellIndex.y / cellSize.y + "_" + cellIndex.z / cellSize.z + ".cell");
        if (f.exists() && !f.isDirectory()) {
            logger.info("patch exists in file system cache. trying to load...");
            // Zelle laden
            InputStream fis = null;
            InputStream cis = null;
            try {
//                patch = (VoxelPatch)new ObjectInputStream(new FileInputStream(f)).readObject();
                fis = new FileInputStream(f);

                if (compressCells)
                    cis = new InflaterInputStream(fis);

                InputStream is = compressCells ? cis : fis;

                ByteArrayOutputStream bos = new ByteArrayOutputStream();

                int len, size = 1024;
                byte[] buf = new byte[size];
                while ((len = is.read(buf, 0, size)) != -1)
                    bos.write(buf, 0, len);

                byte[] barray = bos.toByteArray();

                final int bytesRead = is.read(barray);

                patch = (VoxelPatch) conf.asObject(barray);
            } catch (IOException e) {
                logger.fatal("unable to write patch to file.");
            } finally {
                try {
                    if (fis != null)
                        fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (cis != null)
                        cis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            logger.info("patch does not exist in file system cache. creating and saving...");
            // Zelle erzeugen
            patch = boxedCreator.createCell(mapping, cellIndex);

            // Zelle speichern
            OutputStream fos = null;
            OutputStream cos = null;
            try {
                //new ObjectOutputStream(new FileOutputStream(f)).writeObject(patch);
                byte[] barray = conf.asByteArray(patch);

                fos = new FileOutputStream(f);

                if (compressCells)
                    cos = new DeflaterOutputStream(fos);

                OutputStream os = compressCells ? cos : fos;

                os.write(barray);
            } catch (IOException e) {
                e.printStackTrace();
                logger.fatal("unable to save patch to file.");
            } finally {
                try {
                    if (cos!=null)
                        cos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (fos!=null)
                        fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (patch == null)
            throw new RuntimeException("CachedTerrainCreator was unable to create/deserialize a cell!");

        return patch;
    }

    @Override
    public VectorI3 getCellSize() {
        return boxedCreator.getCellSize();
    }
}
