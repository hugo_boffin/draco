/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model.generator;

import model.Mapping;
import util.math.VectorI3;

/**
 * MaskFunction implements a concrete Masking for a {@link model.generator.MaskableTerrainCreator}.
 * This is primarily useful for a {@link model.FiniteTerrain}.
 *
 * @author  Christian Sommer
 * @since   3/1/2015
 */
public interface MaskFunction {
    /**
     * Default Mask function which does not mask anything out.
     */
    MaskFunction NO_MASK = new MaskFunction() {
        @Override
        public boolean isMaskedOut(VectorI3 mappedCoords) {
            return false;
        }

        @Override
        public boolean isMaskedOut(int x, int y, int z, Mapping mapping) {
            return false;
        }
    };

    /**
     * Checks if a given already mapped coordinate is masked out. Returns true if masked out.
     *
     * @param mappedCoords mapped voxel coordinates
     * @return Masked out?
     */
    boolean isMaskedOut(VectorI3 mappedCoords);

    /**
     * Checks if a given coordinate is masked out. Returns true if masked out.
     *
     * @param x unmapped x
     * @param y unmapped y
     * @param z unmapped z
     * @param mapping Mapping Function
     * @return Masked out?
     */
    boolean isMaskedOut(int x, int y, int z, Mapping mapping);
}
