/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model.generator;

import model.Mapping;
import model.VoxelPatch;

/**
 * WorldToolkit is a base interface for transformation toolkits for concrete world types.
 *
 * A {@link model.generator.BasePassFunction} depends on it. It should hide masking
 * and other low level functions like perlin noise and other filters.
 *
 * @author  Christian Sommer
 * @since   3/3/2015
 */
public interface WorldToolkit {
    /**
     * Returns the cell which should be generated/transformed.
     *
     * @return cell
     */
    VoxelPatch getCell();

    /**
     * Returns the mapping of the cell.
     *
     * @return mapping
     */
    Mapping getMapping();

    /**
     * Returns the active mask function. (if there is any)
     *
     * @return mask function
     */
    MaskFunction getMaskFunction();
}
