/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model.generator;

import model.Mapping;
import model.VoxelPatch;
import util.math.VectorI3;

/**
 * Base interface for all Terrain cell generators.
 *
 * @author  Christian Sommer
 * @since   6/16/2014
 */
public interface TerrainCreator {
    /**
     * Generates a Cell. Could be cpu-intensive...
     *
     * @param mapping (Offset-)Mapping for this cell
     * @param cellIndex index of the cell
     * @return generated cell
     */
    VoxelPatch createCell(Mapping mapping, VectorI3 cellIndex);

    /**
     * Returns the (overall constant) cell size.
     *
     * @return cell size
     */
    VectorI3 getCellSize();
}