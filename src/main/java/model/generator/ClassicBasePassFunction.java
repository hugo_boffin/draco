/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model.generator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import util.StopWatch;

/**
 * Classic BasePassFunction. Can generate cells with perlin noise land, water and sand.
 *
 * @author  Christian Sommer
 * @since   3/2/2015
 */
public class ClassicBasePassFunction implements BasePassFunction {
    private static final Logger logger = LogManager.getLogger(ClassicBasePassFunction.class.getName());
    private final int seed;
    private final boolean fillWater;
    private final int waterLevel;
    private final boolean addSandNearWater;
    private final int sandRadius;
    private final boolean smoothEdges;

    /**
     * Generates cells with water,sand and smooth edges.
     *
     * @param seed world seed
     */
    public ClassicBasePassFunction(int seed) {
        this.fillWater = true;
        this.waterLevel = 6;
        this.addSandNearWater = true;
        this.sandRadius = 1;
        this.smoothEdges = true;
        this.seed = seed;
    }

    /**
     * Generates cells with water,sand and smooth edges.
     *
     * @param seed world seed
     * @param fillWater Should water be filled in?
     * @param waterLevel water line
     * @param addSandNearWater Should sand be added near water?
     * @param sandRadius radius of sand from the nearest water voxel
     * @param smoothEdges Should voxels be smoothed?
     */
    public ClassicBasePassFunction(int seed, boolean fillWater, int waterLevel,
                                   boolean addSandNearWater, int sandRadius, boolean smoothEdges) {
        this.seed = seed;
        this.fillWater = fillWater;
        this.waterLevel = waterLevel;
        this.addSandNearWater = addSandNearWater;
        this.sandRadius = sandRadius;
        this.smoothEdges = smoothEdges;
    }

    @Override
    public void compute(DefaultWorldToolkit toolkit) {
        StopWatch.measureAndLog("fillWithNoise", logger, () -> toolkit.fillWithNoise(seed, 1.0 / 32.0, 1.0 / 16.0));
        if (fillWater) {
            StopWatch.measureAndLog("addWaterLayer", logger, () -> toolkit.addWaterLayer(waterLevel));
        }
        if (addSandNearWater) {
            StopWatch.measureAndLog("placeSandNearWater", logger, () -> toolkit.placeSandNearWater(sandRadius));
        }
        if (smoothEdges) {
            StopWatch.measureAndLog("smoothEdges", logger, toolkit::smoothEdges);
        }
    }
}
