/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model.generator;

import model.Mapping;
import model.VoxelPatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import util.StopWatch;
import util.math.Array3D;
import util.math.Rectangle;
import util.math.VectorI3;

/**
 * Terrain Creator that uses procedural methods like perlin noise or heterogene functions.
 *
 * @author  Christian Sommer
 * @since   7/1/2013
 */
public class ProceduralTerrainCreator implements MaskableTerrainCreator {
    private static final Logger logger = LogManager.getLogger(ProceduralTerrainCreator.class.getName());

    private final VectorI3 cellSize;
    private final BasePassFunction basePassFunction;
    private final LightGenerator lightGenerator;
    private final Rectangle smoothingRectangle = new Rectangle(2,2,2,2);
    private MaskFunction maskFunction = MaskFunction.NO_MASK;

    /**
     * Creates a classic procedural terrain creator.
     *
     * @param width cell width
     * @param height cell height
     * @param depth cell depth
     * @param seed world seed
     */
    public ProceduralTerrainCreator(int width, int height, int depth, int seed) {
        this.cellSize = new VectorI3(width, height, depth);
        this.basePassFunction = new ClassicBasePassFunction(seed);
        this.lightGenerator = new LightGenerator(LightGenerator.LightPassType.OCCLUSION);
    }

    /**
     * Creates a custom procedural terrain creator.
     *
     * @param cellSize cell size
     * @param basePassFunction base pass function
     * @param lightPassType lighting model
     */
    public ProceduralTerrainCreator(VectorI3 cellSize, BasePassFunction basePassFunction, LightGenerator.LightPassType lightPassType) {
        this.cellSize = cellSize;
        this.basePassFunction = basePassFunction;
        this.lightGenerator = new LightGenerator(lightPassType);
    }

    @Override
    public VoxelPatch createCell(Mapping mapping, VectorI3 cellIndex) {
        Rectangle overlapping = lightGenerator.getLightPassType() == LightGenerator.LightPassType.OCCLUSION
                ? lightGenerator.getOverlapping() : smoothingRectangle;

        final Mapping overlapMapping = mapping.createOffset(overlapping.getLeft(), overlapping.getTop());

        final VoxelPatch patch = new VoxelPatch(cellSize.x + (overlapping.getLeft()+overlapping.getRight()),
                cellSize.y + (overlapping.getTop()+overlapping.getBottom()), cellSize.z);
        patch.lightPatch = new Array3D<>(cellSize.x + (overlapping.getLeft()+overlapping.getRight()),
                cellSize.y + (overlapping.getTop()+overlapping.getBottom()), cellSize.z, 0);

        DefaultWorldToolkit toolkit = new DefaultWorldToolkit(patch, overlapMapping, maskFunction);
        StopWatch.measureAndLog("BasePassFunction", logger, () -> basePassFunction.compute(toolkit));
        StopWatch.measureAndLog("Cell Illumination", logger, () -> lightGenerator.illuminateCell(patch, overlapMapping));

        return patch.createSubset(overlapping.getLeft(), overlapping.getTop(), 0,
                cellSize.x + overlapping.getLeft(), cellSize.y + overlapping.getTop(), cellSize.z);
    }

    @Override
    public VectorI3 getCellSize() {
        return cellSize;
    }

    @Override
    public void setMaskFunction(MaskFunction maskFunction) {
        this.maskFunction = maskFunction;
    }
}
