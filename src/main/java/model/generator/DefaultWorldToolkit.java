/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model.generator;

import model.Mapping;
import model.Voxel;
import model.VoxelPatch;
import util.math.Array3D;
import util.math.VectorI3;
import util.noise.ImprovedNoise;

import java.util.Random;
import java.util.TreeSet;

/**
 * Default world toolkit with some generator functions like perlin noise, water flood fill, etc..
 *
 * @author  Christian Sommer
 * @since   3/3/2015
 */
public class DefaultWorldToolkit implements WorldToolkit {
    private static final Voxel baseVoxel = new Voxel(0, 15);
    private static final Voxel greenVoxel = new Voxel(0, 15);
    private static final Voxel sandVoxel = new Voxel(1, 15);
    private static final Voxel waterVoxel = new Voxel(2, 15);

    private final VoxelPatch patch;
    private final Mapping mapping;
    private final MaskFunction maskFunction;

    /**
     * Creates a default world toolkit with the given parameters
     *
     * @param cell the cell we want to work on
     * @param mapping cell mapping
     * @param maskFunction masking function
     */
    public DefaultWorldToolkit(VoxelPatch cell, Mapping mapping, MaskFunction maskFunction) {
        this.patch = cell;
        this.mapping = mapping;
        this.maskFunction = maskFunction;
    }

    /**
     * Fills a cell with perlin noise.
     *
     * @param seed world seed. -1 means random.
     * @param scaleXY noise scaling on the x,y axis
     * @param scaleZ noise scaling on the z axis
     */
    public void fillWithNoise(int seed, double scaleXY, double scaleZ) {
        Random random = new Random();
        random.setSeed(seed);

        final double oX = seed == -1 ? 0 : random.nextDouble();
        final double oY = seed == -1 ? 0 : random.nextDouble();
        final double oZ = seed == -1 ? 0 : random.nextDouble();

        patch.transform((x, y, z, i) -> {
            final VectorI3 mappedCoords = mapping.map(x, y, z);

            if (maskFunction.isMaskedOut(mappedCoords))
                return null;

            if (z == 0) {
                return baseVoxel;
            } else {
                final double noise = ImprovedNoise.noise(oX + mappedCoords.x * scaleXY,
                        oY + mappedCoords.y * scaleXY,
                        oZ + mappedCoords.z * scaleZ);
                final boolean filled = (noise - (double) z / patch.getDepth()) >= 0.0;
                return filled ? greenVoxel : null;
            }
        });
    }

    /**
     * Flood fills water in the cell. Simple iterative flood fill algorithm.
     * Does not flood unconnected holes/dungeons below waterline.
     *
     * @param z water level
     */
    public void addWaterLayer(int z) {
        TreeSet<VectorI3> cells = new TreeSet<>();

        for (int y = 0; y < patch.getHeight(); y++) {
            for (int x = 0; x < patch.getWidth(); x++) {
                if (patch.get(x, y, z) == null && !maskFunction.isMaskedOut(x, y, z, mapping))
                    cells.add(new VectorI3(x, y, z));
            }
        }

        while (!cells.isEmpty()) {
            VectorI3 p = cells.pollFirst();
            patch.set(p.x, p.y, p.z, waterVoxel);

            if (p.z > 0 && patch.get(p.x, p.y, p.z - 1) == null && !maskFunction.isMaskedOut(p.x, p.y, p.z - 1, mapping))
                cells.add(new VectorI3(p.x, p.y, p.z - 1));

            if (p.x > 0 && patch.get(p.x - 1, p.y, p.z) == null && !maskFunction.isMaskedOut(p.x - 1, p.y, p.z, mapping))
                cells.add(new VectorI3(p.x - 1, p.y, p.z));

            if (p.x < patch.getWidth() - 1 && patch.get(p.x + 1, p.y, p.z) == null && !maskFunction.isMaskedOut(p.x + 1, p.y, p.z, mapping))
                cells.add(new VectorI3(p.x + 1, p.y, p.z));

            if (p.y > 0 && patch.get(p.x, p.y - 1, p.z) == null && !maskFunction.isMaskedOut(p.x, p.y - 1, p.z, mapping))
                cells.add(new VectorI3(p.x, p.y - 1, p.z));

            if (p.y < patch.getHeight() - 1 && patch.get(p.x, p.y + 1, p.z) == null && !maskFunction.isMaskedOut(p.x, p.y + 1, p.z, mapping))
                cells.add(new VectorI3(p.x, p.y + 1, p.z));
        }
    }

    /**
     * Adds sand voxels near water.
     *
     * @param r sand radius
     */
    public void placeSandNearWater(int r) {
        patch.transform((x1, y1, z1, i) -> {
            VectorI3 t = mapping.map(x1, y1, z1);
            if (i != null && i.base != waterVoxel.base) {
                Array3D.FoldProjectedFunction<Boolean, Voxel> nearWaterPredicate =
                        (v, e, lx, ly, lz, px, py, pz) ->
                                v || (e != null && e.base == waterVoxel.base);
                if (patch.foldLeft(t.x - r, t.y - r, t.z - r, t.x + r, t.y + r, t.z + r, nearWaterPredicate, false, mapping::unmap))
                    return sandVoxel;
            }
            return i;
        });
    }

    /**
     * Smoothes the entire cell.
     */
    public void smoothEdges() {
        patch.transform(0, 0, 1, patch.getWidth(), patch.getHeight(), patch.getDepth(), (x, y, z, voxel) -> {
            if (voxel != null && (z == patch.getHeight() || (y >= 2 && patch.get(x, y - 2, z + 1) == null))) {
//                if (y < patch.getHeight()-2 && patch.get(x,y+2,z-1) == null)
//                    return voxel;

                int x0 = patch.getLeft(x, y, z) != null ? 1 : 0;
                int x1 = patch.getBottomLeft(x, y, z) != null ? 1 : 0;
                int x2 = patch.getBottom(x, y, z) != null ? 1 : 0;
                int x3 = patch.getBottomRight(x, y, z) != null ? 1 : 0;
                int x4 = patch.getRight(x, y, z) != null ? 1 : 0;
                int x5 = patch.getTopRight(x, y, z) != null ? 1 : 0;
                int x6 = patch.getTop(x, y, z) != null ? 1 : 0;
                int x7 = patch.getTopLeft(x, y, z) != null ? 1 : 0;

                int fullState = x0 + x1 * 2 + x2 * 4 + x3 * 8 +
                        x4 * 16 + x5 * 32 + x6 * 64 + x7 * 128;

                int newIndex = switch (fullState) {
                    // 3 edges above
                    case 0B01111111 -> 14;
                    case 0B11111101 -> 13;
                    case 0B11110111 -> 11;
                    case 0B11011111 -> 7;

                    // 2 edges above

                    // one sided
                    case 0B01111100 -> 12;
                    case 0B11111100 -> 12;
                    case 0B01111110 -> 12;
                    case 0B11110001 -> 9;
                    case 0B11111001 -> 9;
                    case 0B11110011 -> 9;
                    case 0B00011111 -> 6;
                    case 0B00111111 -> 6;
                    case 0B10011111 -> 6;
                    case 0B11000111 -> 3;
                    case 0B11100111 -> 3;
                    case 0B11001111 -> 3;

                    // bridge horizontal / vertical
                    case 0B01110111 -> 10;
                    case 0B11011101 -> 5;

                    // 1 Edge above
                    case 0B11111000 -> 8;
                    case 0B11110000 -> 8;
                    case 0B01111000 -> 8;
                    case 0B01110000 -> 8;
                    case 0B00111100 -> 4;
                    case 0B00111110 -> 4;
                    case 0B00011110 -> 4;
                    case 0B00011100 -> 4;
                    case 0B10001111 -> 2;
                    case 0B00001111 -> 2;
                    case 0B10000111 -> 2;
                    case 0B00000111 -> 2;
                    case 0B11100001 -> 1;
                    case 0B11000001 -> 1;
                    case 0B11100011 -> 1;
                    case 0B11000011 -> 1;

                    // 0 edges
                    case 0B00000000 -> 15;
                    default ->// Something or nothing on all edges
                            15;
                };

                return new Voxel(voxel.base, newIndex);
            } else
                return voxel;
        });
    }

    @Override
    public VoxelPatch getCell() {
        return patch;
    }

    @Override
    public Mapping getMapping() {
        return mapping;
    }

    @Override
    public MaskFunction getMaskFunction() {
        return maskFunction;
    }
}
