/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model;

import model.generator.MaskFunction;
import model.generator.MaskableTerrainCreator;
import model.generator.ProceduralTerrainCreator;
import util.math.RangeI3;
import util.math.VectorI3;
import util.tiles.GroundType;

import java.util.List;

/**
 * Implements a finite terrain through a delegated and masked {@link model.StreamingTerrain}.
 *
 * @author  Christian Sommer
 * @since   6/16/2014
 */
public class FiniteTerrain implements Terrain {
    private static final int DEFAULT_SIZE = 20;
    private final MaskableTerrainCreator boxedCreator;
    private final StreamingTerrain terrain;
    private final RangeI3 diamondMask;
    private final MaskFunction diamondMaskFunction = new MaskFunction() {
        @Override
        public boolean isMaskedOut(VectorI3 mappedCoords) {
            return (mappedCoords.x < diamondMask.min.x || mappedCoords.x > diamondMask.max.x ||
                    mappedCoords.y < diamondMask.min.y || mappedCoords.y > diamondMask.max.y);
        }

        @Override
        public boolean isMaskedOut(int x, int y, int z, Mapping mapping) {
            final VectorI3 mappedCoords = mapping.map(x, y, z);
            return (mappedCoords.x < diamondMask.min.x || mappedCoords.x > diamondMask.max.x ||
                    mappedCoords.y < diamondMask.min.y || mappedCoords.y > diamondMask.max.y);
        }
    };

    /**
     * Creates a finite terrain with default values.
     */
    public FiniteTerrain() {
        this.boxedCreator = new ProceduralTerrainCreator(Terrain.DEFAULT_CELL_SIZE.x, Terrain.DEFAULT_CELL_SIZE.y, Terrain.DEFAULT_CELL_SIZE.z, -1);
        this.terrain = new StreamingTerrain(boxedCreator);
        this.boxedCreator.setMaskFunction(diamondMaskFunction);
        this.diamondMask = new RangeI3(new VectorI3(-DEFAULT_SIZE, -DEFAULT_SIZE, 0),
                new VectorI3(+DEFAULT_SIZE, +DEFAULT_SIZE, 1000));
    }

    /**
     * Creates a finite terrain with the given boxed creators and boundaries.
     *
     * @param boxedCreator boxed creator
     * @param diamondMask boundarys of the mask
     */
    public FiniteTerrain(MaskableTerrainCreator boxedCreator, RangeI3 diamondMask) {
        this.boxedCreator = boxedCreator;
        this.terrain = new StreamingTerrain(boxedCreator);
        this.boxedCreator.setMaskFunction(diamondMaskFunction);
        this.diamondMask = diamondMask;
    }

    @Override
    public Voxel getVoxel(int x, int y, int z) {
        return terrain.getVoxel(x, y, z);
    }

    @Override
    public Voxel getVoxel(VectorI3 pos) {
        return terrain.getVoxel(pos);
    }

    @Override
    public void setVoxel(int x, int y, int z, Voxel value) {
        terrain.setVoxel(x,y,z,value);
    }

    @Override
    public void setVoxel(VectorI3 pos, Voxel value) {
        terrain.setVoxel(pos, value);
    }

    @Override
    public Mapping getMapping() {
        return terrain.getMapping();
    }

    @Override
    public VectorI3 getStartingPosition() {
        return terrain.getStartingPosition();
    }

    @Override
    public List<GroundType> getVoxelTypes() {
        return terrain.getVoxelTypes();
    }

    @Override
    public int getCachedCellCount() {
        return terrain.getCachedCellCount();
    }

    @Override
    public int getCachedDoneCellCount() {
        return terrain.getCachedDoneCellCount();
    }

    @Override
    public void shutDownExecutor() {
        terrain.shutDownExecutor();
    }

    @Override
    public VoxelPatch getPatch(VectorI3 cellIndex) {
        return terrain.getPatch(cellIndex);
    }

    @Override
    public void warmupPatch(VectorI3 cellIndex) {
        terrain.warmupPatch(cellIndex);
    }

    @Override
    public VectorI3 getCellSize() {
        return terrain.getCellSize();
    }
}
