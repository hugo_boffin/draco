/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model;

import util.math.Array3D;

import java.io.Serializable;

/**
 * Three-dimensional voxel array.
 *
 * @author  Christian Sommer
 * @since   7/1/2013
 */
public class VoxelPatch extends Array3D<Voxel> implements Serializable {
    /**
     * Voxel used for out of range requests.
     */
    public static final Voxel EDGE_VOXEL = new Voxel(0, 0);

    /**
     * Light Patch
     */
    public Array3D<Integer> lightPatch;

    /**
     * Constructor for serialization. <b>NEVER use this in handwritten code!</b>
     */
    private VoxelPatch() {
    }

    /**
     * Creates a cell with the given size. Cell will be completely empty.
     *
     * @param width cell width
     * @param height cell height
     * @param depth cell depth
     */
    public VoxelPatch(int width, int height, int depth) {
        super(width, height, depth, (x, y, z) -> null);
    }

    /**
     * Creates a cell with the given size and initializes it with the given function.
     *
     * @param width cell width
     * @param height cell height
     * @param depth cell depth
     * @param initFunc init function
     */
    public VoxelPatch(int width, int height, int depth, InitFunction<Voxel> initFunc) {
        super(width, height, depth, initFunc);
    }

    /**
     * Return true if a voxel exists at the given coordinates
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @return is filled
     */
    public boolean isFilled(int x, int y, int z) {
        return get(x, y, z) != null;
    }

    /**
     * Returns the overall fill count of the cell
     *
     * @return fill count
     */
    public int getFillCount() {
        int voxelCount = 0;
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
                for (int z = 0; z < depth; z++)
                    if (isFilled(x, y, z))
                        voxelCount++;
        return voxelCount;
    }

    /**
     * Returns the voxel to the left for the given coordinates.
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @return voxel to the left
     */
    public Voxel getLeft(int x, int y, int z) {
        if (y % 2 == 0)
            return getVoxelSafe(x - 1, y + 1, z);
        else
            return getVoxelSafe(x, y + 1, z);
    }

    /**
     * Returns the voxel to the right for the given coordinates.
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @return voxel to the right
     */
    public Voxel getRight(int x, int y, int z) {
        if (y % 2 == 0)
            return getVoxelSafe(x, y - 1, z);
        else
            return getVoxelSafe(x + 1, y - 1, z);
    }

    /**
     * Returns the voxel to the top for the given coordinates.
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @return voxel to the top
     */
    public Voxel getTop(int x, int y, int z) {
        if (y % 2 == 0)
            return getVoxelSafe(x - 1, y - 1, z);
        else
            return getVoxelSafe(x, y - 1, z);
    }

    /**
     * Returns the voxel to the bottom for the given coordinates.
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @return voxel to the bottom
     */
    public Voxel getBottom(int x, int y, int z) {
        if (y % 2 == 0)
            return getVoxelSafe(x, y + 1, z);
        else
            return getVoxelSafe(x + 1, y + 1, z);
    }

    /**
     * Returns the voxel to the top-left for the given coordinates.
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @return voxel to the top-left
     */
    public Voxel getTopLeft(int x, int y, int z) {
        return getVoxelSafe(x - 1, y, z);
    }

    /**
     * Returns the voxel to the bottom-left for the given coordinates.
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @return voxel to the bottom-left
     */
    public Voxel getBottomLeft(int x, int y, int z) {
        return getVoxelSafe(x, y + 2, z);
    }

    /**
     * Returns the voxel to the top-right for the given coordinates.
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @return voxel to the top-right
     */
    public Voxel getTopRight(int x, int y, int z) {
        return getVoxelSafe(x, y - 2, z);
    }

    /**
     * Returns the voxel to the bottom-right for the given coordinates.
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @return voxel to the bottom-right
     */
    public Voxel getBottomRight(int x, int y, int z) {
        return getVoxelSafe(x + 1, y, z);
    }

    /**
     * Returns a voxel. I dont like this function.
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @return voxel, EDGE_VOXEL if out-of-range.
     */
    private Voxel getVoxelSafe(int x, int y, int z) {
        if (x < 0 || y < 0 || z < 0 || x >= getWidth() || y >= getHeight() || z >= getDepth())
            return EDGE_VOXEL;
        return get(x, y, z);
    }

    /**
     * Creates a subset of this cell.
     *
     * @param x0 minimal x
     * @param y0 minimal y
     * @param z0 minimal z
     * @param x1 maximal x
     * @param y1 maximal y
     * @param z1 maximal z
     * @return cell subset
     */
    public VoxelPatch createSubset(int x0, int y0, int z0, int x1, int y1, int z1) {
        VoxelPatch subPatch = new VoxelPatch(x1 - x0, y1 - y0, z1 - z0, (x, y, z) -> this.get(x + x0, y + y0, z + z0));
        if (this.lightPatch != null) {
            subPatch.lightPatch = new Array3D<>(x1 - x0, y1 - y0, z1 - z0, (x, y, z) -> this.lightPatch.get(x + x0, y + y0, z + z0));
        }
        return subPatch;
    }
}
