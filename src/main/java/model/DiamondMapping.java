/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model;

import util.math.VectorI3;

/**
 * Provides a Mapping between the real data structure coordinates and the
 * more intuitive diamond style coordinates of an isometric map.
 *
 * @author  Christian Sommer
 * @since   5/13/2014
 */
public class DiamondMapping implements Mapping {
    private final int offsetX;
    private final int offsetY;

    /**
     * Creates a mapping with offsets.
     *
     * @param offsetX x-offset
     * @param offsetY y-offset
     */
    public DiamondMapping(int offsetX, int offsetY) {
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }

    /**
     * Creates a mapping without offset
     */
    public DiamondMapping() {
        this.offsetX = 0;
        this.offsetY = 0;
    }

    @Override
    public VectorI3 map(int x, int y, int z) {
        x -= offsetX;
        y -= offsetY;
        y += 2 * z;

        return new VectorI3(x - y / 2 - (y < 0 ? y % 2 : 0),
                x + y / 2 + (y > 0 ? y % 2 : 0),
                z);
    }

    @Override
    public VectorI3 map(VectorI3 raw) {
        return map(raw.x, raw.y, raw.z);
    }

    @Override
    public VectorI3 unmap(int x, int y, int z) {
        final int ry = y - x;
        final int rx = x + ry / 2 + (ry < 0 ? ry % 2 : 0);

        return new VectorI3(
                rx + offsetX,
                ry - 2 * z + offsetY,
                z);
    }

    @Override
    public VectorI3 unmap(VectorI3 mapped) {
        return unmap(mapped.x, mapped.y, mapped.z);
    }

    @Override
    public Mapping createOffset(int x, int y) {
        if (x % 2 != 0 || y % 2 != 0) {
            throw new RuntimeException("DiamondMapping does not allow uneven offsets!");
        }
        return new DiamondMapping(offsetX + x, offsetY + y);
    }
}
