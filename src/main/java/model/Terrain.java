/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model;

import util.math.VectorI3;
import util.tiles.GroundType;

import java.util.List;

/**
 * Base Interface for all Terrain types.
 *
 * @author  Christian Sommer
 * @since   6/16/2014
 */
public interface Terrain {
    /**
     * Default value for the cell size.
     */
    VectorI3 DEFAULT_CELL_SIZE = new VectorI3(32, 128, 32);

    /**
     * returns the cell for the given index.
     *
     * @param cellIndex index of the cell
     * @return cell for the given index.
     */
    VoxelPatch getPatch(VectorI3 cellIndex);

    /**
     * Pushes a terrain cell onto the LRU Cache, but does not wait for it being loaded.
     *
     * @param cellIndex coordinates of given cell
     */
    void warmupPatch(VectorI3 cellIndex);

    /**
     * Returns the cell size in voxel units.
     *
     * @return cell size
     */
    VectorI3 getCellSize();

    /**
     * Returns the voxel for the given coordinates
     * <p>
     * INFO: Input and Output Coordinate System: Real System, _not_ naive!
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @return voxel object
     */
    Voxel getVoxel(int x, int y, int z);

    /**
     * Returns the voxel for the given coordinates
     * <p>
     * INFO: Input and Output Coordinate System: Real System, _not_ naive!
     *
     * @param pos coordinate vector
     * @return voxel object
     */
    Voxel getVoxel(VectorI3 pos);

    /**
     * Sets the voxel for a given coordinates
     * <p>
     * INFO: Input and Output Coordinate System: Real System, _not_ naive!
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @param value new value
     */

    void setVoxel(int x, int y, int z, Voxel value);
    /**
     * Sets the voxel for a given coordinates
     * <p>
     * INFO: Input and Output Coordinate System: Real System, _not_ naive!
     *
     * @param pos coordinate vector
     * @param value new value
     */
    void setVoxel(VectorI3 pos, Voxel value);

    /**
     * Returns the used mapping inside the cell
     *
     * @return mapping function
     */
    Mapping getMapping();

    /**
     * Returns the starting position for the player
     *
     * @return starting position
     */
    VectorI3 getStartingPosition();

    /**
     * Returns the list of all possible voxel types.
     *
     * @return voxel type list
     */
    List<GroundType> getVoxelTypes();

    /**
     * Get the number of cells in the LRU Cache
     *
     * @return number of cached cells
     */
    int getCachedCellCount();

    /**
     * Get the number of finished cells in the LRU Cache
     *
     * @return number of finished cells
     */
    int getCachedDoneCellCount();

    /**
     * Shuts down Executor Service / Thread Pool
     */
    void shutDownExecutor();
}
