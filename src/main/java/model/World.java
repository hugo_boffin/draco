/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model;

/**
 * World Model class. This class is the main entry class for our game model.
 *
 * @author  Christian Sommer
 * @since   6/16/2014
 */
public class World {
    private final String name;
    private final Terrain terrain;
    private final Player player;

    /**
     * Creates a default World
     */
    public World() {
        this.terrain = new StreamingTerrain();
        this.name = "Unnamed world";
        this.player = new Player(terrain.getStartingPosition());
    }

    /**
     * Creates a custom world
     *
     * @param name world name
     * @param terrain world terrain
     */
    public World(String name, Terrain terrain) {
        this.name = name;
        this.terrain = terrain;
        this.player = new Player(terrain.getStartingPosition());
    }

    /**
     * Returns the world name.
     *
     * @return world name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the terrain.
     *
     * @return terrain
     */
    public Terrain getTerrain() {
        return terrain;
    }

    /**
     * Returns the player
     *
     * @return player
     */
    public Player getPlayer() {
        return player;
    }
}
