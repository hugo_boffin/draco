/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model;

import util.math.VectorI3;

/**
 * Player Model Implementation.
 *
 * @author  Christian Sommer
 * @since   7/11/2014
 */
public class Player {
    private final String name;
    private VectorI3 position;

    /**
     * Create a player at the given position.
     *
     * @param position player position.
     */
    public Player(VectorI3 position) {
        this.position = position;
        this.name = "Hannes";
    }

    /**
     * Return the player position.
     *
     * @return player position.
     */
    public VectorI3 getPosition() {
        return position;
    }

    /**
     * Set the player position.
     *
     * @param position player position.
     */
    public void setPosition(VectorI3 position) {
        this.position = position;
    }
}
