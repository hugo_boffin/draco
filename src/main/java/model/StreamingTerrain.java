/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model;

import model.generator.ProceduralTerrainCreator;
import model.generator.TerrainCreator;
import org.apache.commons.collections4.map.LRUMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import util.StopWatch;
import util.math.VectorI3;
import util.tiles.GroundType;
import util.tiles.Tiles;

import java.util.List;
import java.util.concurrent.*;

/**
 * Terrain Implementation that supports endless(...) streaming of voxel cells.
 * Patches are generated through a TerrainCreator
 *
 * @author  Christian Sommer
 * @since   6/16/2014
 */
public class StreamingTerrain implements Terrain {
    private static final Logger logger = LogManager.getLogger(StreamingTerrain.class.getName());
    private final TerrainCreator creator;
    private final DiamondMapping mapping = new DiamondMapping();
    private final LRUMap<VectorI3, Future<VoxelPatch>> patchCache = new LRUMap<>(256);
    private final ExecutorService executor = Executors.newFixedThreadPool(2, new SimpleThreadFactory());

    /**
     * Create a streamed terrain with a classic procedural terrain creator and default cell size.
     */
    public StreamingTerrain() {
        creator = new ProceduralTerrainCreator(Terrain.DEFAULT_CELL_SIZE.x, Terrain.DEFAULT_CELL_SIZE.y, Terrain.DEFAULT_CELL_SIZE.z, -1);
    }

    /**
     * Create a streamed terrain with a custom {@link model.generator.TerrainCreator}.
     *
     * @param creator custom TerrainCreator
     */
    public StreamingTerrain(TerrainCreator creator) {
        this.creator = creator;
    }

    @Override
    public VoxelPatch getPatch(VectorI3 cellIndex) {
        Future<VoxelPatch> patch = patchCache.get(cellIndex);

        if (patch == null) {
//            logger.warn("Cell "+ cellIndex +" was not precached ...");
            patch = loadPatch(cellIndex);
        }

        if (!patch.isDone()) {
            logger.warn("Cell {} has not finished yet ...", cellIndex);
        }

        try {
            return patch.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void warmupPatch(VectorI3 cellIndex) {
        if (!patchCache.containsKey(cellIndex))
            patchCache.put(cellIndex, loadPatch(cellIndex));
    }

    @Override
    public VectorI3 getCellSize() {
        return creator.getCellSize();
    }

    @Override
    public Voxel getVoxel(int x, int y, int z) {
        // 1) Find the right patch
        // 2) Find the right voxel (mapping!)

        // This is to compensate "wrong" round when position has negative values (-7 / 64 = 0)
        final VectorI3 cellIndex = new VectorI3(x / getCellSize().x - (x < 0 ? 1 : 0),
                y / getCellSize().y - (y < 0 ? 1 : 0),
                z / getCellSize().z - (z < 0 ? 1 : 0));
        final VectorI3 relPos = new VectorI3(x % getCellSize().x + (x < 0 ? getCellSize().x : 0),
                y % getCellSize().y + (y < 0 ? getCellSize().y : 0),
                z % getCellSize().z + (z < 0 ? getCellSize().z : 0));

        Voxel v = null;

        v = getPatch(cellIndex).get(relPos.x, relPos.y, relPos.z);

        return v;
    }

    @Override
    public Voxel getVoxel(VectorI3 pos) {
        return getVoxel(pos.x, pos.y, pos.z);
    }

    @Override
    public void setVoxel(int x, int y, int z, Voxel value) {
        // 1) Find the right patch
        // 2) Find the right voxel (mapping!)

        // This is to compensate "wrong" rounding when position has negative values (-7 / 64 = 0)
        final VectorI3 cellIndex = new VectorI3(x / getCellSize().x - (x < 0 ? 1 : 0),
                y / getCellSize().y - (y < 0 ? 1 : 0),
                z / getCellSize().z - (z < 0 ? 1 : 0));
        final VectorI3 relPos = new VectorI3(x % getCellSize().x + (x < 0 ? getCellSize().x : 0),
                y % getCellSize().y + (y < 0 ? getCellSize().y : 0),
                z % getCellSize().z + (z < 0 ? getCellSize().z : 0));

        final VoxelPatch vp = getPatch(cellIndex);

        vp.set(relPos.x, relPos.y, relPos.z,value);
    }

    @Override
    public void setVoxel(VectorI3 pos, Voxel value) {
        setVoxel(pos.x, pos.y, pos.z, value);
    }

    @Override
    public Mapping getMapping() {
        return mapping;
    }

    @Override
    public VectorI3 getStartingPosition() {
        int posX = 0;
        int posY = 0;
        int z = creator.getCellSize().z;

        Voxel v;
        do {
            z--;
            v = getVoxel(mapping.unmap(posX, posY, z)); // Diamond -> Real
        } while (z > 0 && (v == null || v == VoxelPatch.EDGE_VOXEL));

        return mapping.unmap(posX, posY, z); // Diamond -> Real
    }

    @Override
    public List<GroundType> getVoxelTypes() {
        return Tiles.DEFAULT;
    }

    private Future<VoxelPatch> loadPatch(VectorI3 cellIndex) {
        logger.info("Requesting Terrain Cell " + cellIndex.toString());

        final VectorI3 cellSize = creator.getCellSize();

        final FutureTask<VoxelPatch> future = new FutureTask<>(() -> {
            StopWatch watch = new StopWatch();
            watch.start();
            final VectorI3 cellOffset = new VectorI3(
                    cellIndex.x * -cellSize.x,
                    cellIndex.y * -cellSize.y,
                    cellIndex.z * -cellSize.z);
            VoxelPatch patch = creator.createCell(mapping.createOffset(cellOffset.x, cellOffset.y), cellIndex);
            watch.stop();
            logger.info("Loading/Generating Terrain Cell {} took {}ms",cellIndex, String.format("%.2f", watch.time()));
            return patch;
        });

        patchCache.put(cellIndex, future);

        executor.execute(future);

        return future;
    }

    @Override
    public int getCachedCellCount() {
        return patchCache.size();
    }

    @Override
    public int getCachedDoneCellCount() {
        class CellInfo {
            public int cellCount = 0;
            public int cellDoneCount = 0;
        }
        final CellInfo cellInfo = new CellInfo();

        patchCache.forEach((vectorI3, voxelPatchFuture) -> {
            if (voxelPatchFuture.isDone())
                cellInfo.cellDoneCount++;
            cellInfo.cellCount++;
        });
        return cellInfo.cellDoneCount;
    }

    @Override
    public void shutDownExecutor() {
        logger.info("Shutting down Thread Pool...");
        executor.shutdown();
    }

    private static class SimpleThreadFactory implements ThreadFactory {
        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r, "StreamingTerrain-" + nextThreadNum());
            t.setPriority(Thread.MIN_PRIORITY);
            return t;
        }

        /* For autonumbering anonymous threads. */
        private static int threadInitNumber;
        private static synchronized int nextThreadNum() {
            return threadInitNumber++;
        }
    }
}
