/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model;

import java.io.Serializable;

/**
 * Implements an immutable voxel. A voxel has a base type and an index inside this base type.
 *
 * @author  Christian Sommer
 * @since   7/1/2013
 */
public class Voxel implements Serializable {
    /**
     * Base Type of the voxel (e.g. sand, grass, ..)
     */
    public final byte base;

    /**
     * (tile) index of the base type
     */
    public final byte index;

    /**
     * Constructor for serialization. <b>NEVER use this in handwritten code!</b>
     */
    public Voxel() {
        base = -1;
        index = -1;
    }

    /**
     * Creates a voxel.
     *
     * @param base base type
     * @param index index
     */
    public Voxel(int base, int index) {
        this.base = (byte) base;
        this.index = (byte) index;
    }

    /**
     * Creates a voxel.
     *
     * @param base base type
     * @param index index
     */
    public Voxel(byte base, byte index) {
        this.base = base;
        this.index = index;
    }

    @Override
    public String toString() {
        return "Voxel [base=" + base + ", index=" + index + "]";
    }
}
