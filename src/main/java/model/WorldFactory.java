/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model;

import groovy.lang.GroovyShell;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

/**
 * World Factory Implementation.
 * <p>
 * Tries to load world configuration from a groovy script. create a default one otherwise.
 *
 * @author  Christian Sommer
 * @since   7/6/2014
 */

public class WorldFactory {
    private static final Logger logger = LogManager.getLogger(WorldFactory.class.getName());

    /**
     * Factory Method. Tries to load world configuration from a groovy script. create a default one otherwise.
     *
     * @return World Instance
     */
    public static World create() {
        final File configFile = new File("world.groovy");
        World world;

        try {
            GroovyShell shell = new GroovyShell();
            String[] list = new String[0];
            world = (World) shell.run(configFile, list);
        } catch (Exception e) {
            logger.error("Unable to load config file. Creating default world ...");

            world = new World();
        }

        return world;
    }
}
