/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model;

import util.math.VectorI3;

/**
 * Base Interface for Mappings between two coordinate systems.
 *
 * @author  Christian Sommer
 * @since   2/25/2015
 */
public interface Mapping {
    /**
     * Real Coordinates {@literal ->} Mapped Coordinates
     *
     * @param x unmapped x
     * @param y unmapped y
     * @param z unmapped z
     * @return mapped coordinates
     */
    VectorI3 map(int x, int y, int z);

    /**
     * Real Coordinates {@literal ->} Mapped Coordinates
     *
     * @param raw unmapped coordinates
     * @return mapped coordinates
     */
    VectorI3 map(VectorI3 raw);

    /**
     * Mapped Coordinates {@literal ->} Real Coordinates
     *
     * @param x mapped x
     * @param y mapped y
     * @param z mapped z
     * @return unmapped coordinates
     */
    VectorI3 unmap(int x, int y, int z);

    /**
     * Mapped Coordinates {@literal ->} Real Coordinates
     *
     * @param mapped mapped coordinates
     * @return unmapped coordinates
     */
    VectorI3 unmap(VectorI3 mapped);

    /**
     * Creates a mapping with an offset from this instance (e.g. for cells)
     *
     * @param x x-offset
     * @param y y-offset
     * @return mapping with an offset
     */
    Mapping createOffset(int x, int y);
}
