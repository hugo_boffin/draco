package picking;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.util.awt.TextRenderer;
import engine.Game;
import engine.renderer.RendererOptions;
import engine.renderer.blitter.Blitter;
import engine.renderer.blitter.Map;
import engine.renderer.blitter.MapFactory;
import model.Voxel;
import model.World;
import util.math.VectorF3;
import util.math.VectorI2;
import util.math.VectorI3;
import util.tiles.GroundType;
import util.tiles.TileGenerator;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class MousePicking {
    private final Game game;
    private GL2 gl;
    private final World world;
    private final Blitter blitter;

    private static class RaycastInfo {
        public int z;
        public Voxel v;
    }

    private enum PickedCorner {
        CENTER,
        TOPLEFT,
        TOPRIGHT,
        BOTTOMLEFT,
        BOTTOMRIGHT,
        UNKNOWN
    }

    public static ArrayList<VectorI3> highlights = new ArrayList<>();
    private final Map mouseVoxelHightlightMap;
    private final BufferedImage pickingMask;
    private VectorI3 mouseVoxel;

    public MousePicking(Game game, GL2 gl, World world, Blitter blitter) {
        this.game = game;
        this.world = world;
        this.blitter = blitter;
        final TileGenerator tg = new TileGenerator(RendererOptions.get().voxelRenderSize, false, false, true, new Color(0, 0, 0, 50), false);
        mouseVoxelHightlightMap = MapFactory.createMapFromBufferedImage(gl,
                tg.renderTile(new GroundType("", new Color(255, 255, 255, 50),
                        new Color(255, 255, 255, 50),
                        new Color(255, 255, 255, 50))), 4, 4);

        final TileGenerator tgo = new TileGenerator(RendererOptions.get().voxelRenderSize, false, false, true, Color.WHITE, true);
        pickingMask = tgo.renderTile(new GroundType("picking", Color.WHITE, Color.WHITE, Color.WHITE));
    }

    public VectorI3 getVoxelUnderMouse() {
        return mouseVoxel;
    }

    private boolean raycast(int startZ, int minZ, RaycastInfo info, int x, int y) {
        int z = startZ;

        Voxel v = null;
        while (v == null && z > minZ) {
            z--;
            v = world.getTerrain().getVoxel(x, y, z);
        }

        info.z = z;
        info.v = v;

        if (v != null)
            highlights.add(new VectorI3(x, y, z));

        return (v != null);
    }

    private PickedCorner getAreaUnderMouse(int x, int y) {
        int color = pickingMask.getRGB(x, y);

        return switch (color) {  // ARGB
            case 0xFFFFFFFF -> PickedCorner.CENTER;
            case 0xFFFF0000 -> PickedCorner.TOPLEFT;
            case 0xFFFFFF00 -> PickedCorner.TOPRIGHT;
            case 0xFF00FF00 -> PickedCorner.BOTTOMLEFT;
            case 0xFF0000FF -> PickedCorner.BOTTOMRIGHT;
            default -> PickedCorner.UNKNOWN;
        };
    }

    public void findVoxelUnderMouse(GL2 gl, GLAutoDrawable drawable, TextRenderer textRenderer, VectorF3 cameraPosition) {
        // TODO: Passthrough Voxels (nonsolid/partly transparent voxels) -> check voxel underneath until positive hit

        highlights.clear();

        final int voxelSize = RendererOptions.get().voxelRenderSize;

        final VectorI2 projMousePos = new VectorI2((int) cameraPosition.x + game.getInput().getMouseX(),
                (int) cameraPosition.y + game.getInput().getMouseY());

        final VectorI2 projInVoxelPos = new VectorI2(
                +projMousePos.x % voxelSize + (projMousePos.x < 0 ? voxelSize : 0),
                +projMousePos.y % (voxelSize / 2) + (projMousePos.y < 0 ? (voxelSize / 2) : 0));

        int cx = (projMousePos.x - (projMousePos.x < 0 ? voxelSize : 0)) / voxelSize;
        int cy = (projMousePos.y - (projMousePos.y < 0 ? voxelSize : 0)) / (voxelSize / 2);
        cx *= 2;
        cy *= 2;

        RaycastInfo baseVoxelInfo = new RaycastInfo();
        raycast(world.getTerrain().getCellSize().z, 0, baseVoxelInfo, cx / 2, cy);

        RaycastInfo topVoxelInfo = new RaycastInfo();
        raycast(world.getTerrain().getCellSize().z, baseVoxelInfo.z, topVoxelInfo, cx / 2, cy - 2);

        if (topVoxelInfo.z > baseVoxelInfo.z) {
            cy -= 2;
            baseVoxelInfo = topVoxelInfo;
        }

        if (baseVoxelInfo.v == null)
            return;

        //TODO: Z_Scale fuer Voxel
        //TODO: Center - Koennte auch mitte unten, links unten oder rechts unten sein - kann man das im tile enkodieren?
        //TODO: Map ist unnnoetig, linke haelfte und transparent? links oben usw.
        //TODO: VoxelPatch : TopIndex

        PickedCorner corner = getAreaUnderMouse((baseVoxelInfo.v.index % 4) * voxelSize + projInVoxelPos.x, //TODO: grenzwert check
                (baseVoxelInfo.v.index / 4) * voxelSize + projInVoxelPos.y);

        highlights.clear();

        switch (corner) {
            case TOPLEFT -> {
                {
                    RaycastInfo topLeftInfo = new RaycastInfo();
                    raycast(world.getTerrain().getCellSize().z, 0, topLeftInfo, ((cx - 1) / 2), cy - 1);

                    RaycastInfo topLeftTopVoxelInfo = new RaycastInfo();
                    raycast(world.getTerrain().getCellSize().z, topLeftInfo.z, topLeftTopVoxelInfo, ((cx - 1) / 2), cy - 3);

                    if (topLeftTopVoxelInfo.v != null && topLeftTopVoxelInfo.z > topLeftInfo.z) {
                        cy -= 2;
                        baseVoxelInfo = topLeftTopVoxelInfo;
                    } else {
                        baseVoxelInfo = topLeftInfo;
                    }
                }
                cx--;
                cy--;
            }
            case TOPRIGHT -> {
                {
                    RaycastInfo toRightInfo = new RaycastInfo();
                    raycast(world.getTerrain().getCellSize().z, 0, toRightInfo, ((cx + 1) / 2), cy - 1);

                    RaycastInfo topRightTopVoxelInfo = new RaycastInfo();
                    raycast(world.getTerrain().getCellSize().z, toRightInfo.z, topRightTopVoxelInfo, ((cx + 1) / 2), cy - 3);

                    if (topRightTopVoxelInfo.v != null && topRightTopVoxelInfo.z > toRightInfo.z) {
                        cy -= 2;
                        baseVoxelInfo = topRightTopVoxelInfo;
                    } else {
                        baseVoxelInfo = toRightInfo;
                    }
                }
                cx++;
                cy--;
            }
            case CENTER -> {
                if (projInVoxelPos.x < voxelSize / 2) {
                    // Links

                    RaycastInfo newVoxelInfo = new RaycastInfo();
                    raycast(world.getTerrain().getCellSize().z, 0, newVoxelInfo, ((cx - 1) / 2), cy + 1);

                    RaycastInfo newTopVoxelInfo = new RaycastInfo();
                    raycast(world.getTerrain().getCellSize().z, newVoxelInfo.z, newTopVoxelInfo, ((cx - 1) / 2), cy + 1 - 2);

                    PickedCorner corner2;
                    if (newTopVoxelInfo.v != null && newTopVoxelInfo.z > newVoxelInfo.z) {
                        cy -= 2;
                        newVoxelInfo = newTopVoxelInfo;
                        corner2 = getAreaUnderMouse(
                                (newVoxelInfo.v.index % 4) * voxelSize + projInVoxelPos.x + voxelSize / 2,
                                (newVoxelInfo.v.index / 4) * voxelSize + projInVoxelPos.y - voxelSize / 4 - +voxelSize / 2); // verkehrt rum?
                    } else {
//                            newVoxelInfo = newVoxelInfo;
                        corner2 = getAreaUnderMouse(
                                (newVoxelInfo.v.index % 4) * voxelSize + projInVoxelPos.x + voxelSize / 2,
                                (newVoxelInfo.v.index / 4) * voxelSize + projInVoxelPos.y - voxelSize / 4);
                    }

                    if (corner2 == PickedCorner.CENTER) {
                        highlights.clear();
                        cx--;
                        cy++;
                    }
                } else {
                    // Rechts
                    RaycastInfo newVoxelInfo = new RaycastInfo();
                    raycast(world.getTerrain().getCellSize().z, 0, newVoxelInfo, ((cx + 1) / 2), cy + 1);

                    RaycastInfo newTopVoxelInfo = new RaycastInfo();
                    raycast(world.getTerrain().getCellSize().z, newVoxelInfo.z, newTopVoxelInfo, ((cx + 1) / 2), cy + 1 - 2);

                    PickedCorner corner2;
                    if (newTopVoxelInfo.v != null && newTopVoxelInfo.z > newVoxelInfo.z) {
                        cy -= 2;
                        newVoxelInfo = newTopVoxelInfo;
                        corner2 = getAreaUnderMouse(
                                (newVoxelInfo.v.index % 4) * voxelSize + projInVoxelPos.x - voxelSize / 2,
                                (newVoxelInfo.v.index / 4) * voxelSize + projInVoxelPos.y - voxelSize / 4 - +voxelSize / 2); // verkehrt rum?
                    } else {
//                            newVoxelInfo = newVoxelInfo;
                        corner2 = getAreaUnderMouse(
                                (newVoxelInfo.v.index % 4) * voxelSize + projInVoxelPos.x - voxelSize / 2,
                                (newVoxelInfo.v.index / 4) * voxelSize + projInVoxelPos.y - voxelSize / 4);
                    }

                    if (corner2 == PickedCorner.CENTER) {
                        highlights.clear();
                        cx++;
                        cy++;
                    }
                }

                // TODO: Test voxel im Sueden (Sueden+1 lohnt nicht, das ist der ursprungsvoxel)
            }
        }


        mouseVoxel = new VectorI3(cx / 2, cy, baseVoxelInfo.z);
        highlights.add(mouseVoxel);

        final VectorI2 projVoxelPos = new VectorI2(
                cx * (voxelSize / 2) - (int) cameraPosition.x,
                cy * (voxelSize / 4) - (int) cameraPosition.y);

        String mouseInfo = String.format("%s %s %s", mouseVoxel, projInVoxelPos, corner);
        blitter.blit(mouseVoxelHightlightMap, projVoxelPos.x, projVoxelPos.y, Color.WHITE, baseVoxelInfo.v != null ? baseVoxelInfo.v.index : 15);
        blitter.flush(gl);

        renderMouseInfo(textRenderer, drawable, mouseInfo);
    }

    private void renderMouseInfo(TextRenderer textRenderer, GLAutoDrawable drawable, String text) {
        final Rectangle2D bounds = textRenderer.getBounds(text);
        textRenderer.beginRendering(drawable.getSurfaceWidth(), drawable.getSurfaceHeight());
        textRenderer.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        textRenderer.draw(text, game.getInput().getMouseX() + 25,
                drawable.getSurfaceHeight() - game.getInput().getMouseY() - (int) bounds.getHeight() - 25);
        textRenderer.endRendering();
    }
}
