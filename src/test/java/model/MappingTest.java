/*
 * Copyright 2013-2015 Christian Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package model;

import org.testng.Assert;
import org.testng.annotations.Test;
import util.math.VectorI3;

import java.util.stream.IntStream;

/**
 * Tests Mapping between real world coordinate and various projections.
 *
 * @author  Christian Sommer
 * @since   10/17/2014
 */
public class MappingTest {
    /**
     * Tests if DiamondMapping is working correctly, since this is an essential part of our world generation
     * The test iterates over various offsets and coordinates, maps them and unmaps the result.
     */
    @Test
    public void testDiamondMapping() {
        int minO = -15;
        int maxO = +15;
        final int min = -10;
        final int max = +10;

        IntStream.rangeClosed(minO, maxO).forEach(ox ->
                IntStream.rangeClosed(minO, maxO).forEach(oy -> {
                            DiamondMapping mapping = new DiamondMapping(ox, oy);
                            IntStream.rangeClosed(min, max).forEach(
                                    x -> IntStream.rangeClosed(min, max).forEach(
                                            y -> IntStream.rangeClosed(min, max).forEach(
                                                    z -> {
                                                        VectorI3 position = new VectorI3(x, y, z);
                                                        VectorI3 position_diamond = mapping.map(position);
                                                        VectorI3 position_back = mapping.unmap(position_diamond);

                                                        Assert.assertEquals(position, position_back, position + "->" +
                                                                position_diamond + "->" + position_back);
                                                    }
                                            )
                                    )
                            );
                        }
                ));
    }
}
