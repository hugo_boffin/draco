import model.StreamingTerrain
import model.Terrain
import model.World
import model.generator.ClassicBasePassFunction
import model.generator.FlatBasePassFunction
import model.generator.LightGenerator
import model.generator.ProceduralTerrainCreator
import util.math.VectorI3

// Default world configurations

//
//def world = new World("Flat World", new StreamingTerrain(
//        new ProceduralTerrainCreator(
//                new VectorI3(32,128,1),
//                new FlatBasePassFunction(),
//                LightGenerator.LightPassType.SIMPLE
//        )
//)
//);
def world = new World("Flat World", new StreamingTerrain(
        new ProceduralTerrainCreator(
                Terrain.DEFAULT_CELL_SIZE,
                new ClassicBasePassFunction(-1), // Kein Seed
                LightGenerator.LightPassType.OCCLUSION
        )
)
)

return world